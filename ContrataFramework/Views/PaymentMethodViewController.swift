//
//  PaymentMethodViewController.swift
//  EstrategiaDigital
//
//  Created by Charls Salazar on 21/07/17.
//  Copyright © 2017 Charls Salazar. All rights reserved.
//

import UIKit
import BaseClases
public protocol PaymentMethodDelegate : NSObjectProtocol {
    func onSuccessLoadInfoPayment(metodoPagoBean : MetodoPagoBean)
}

public class PaymentMethodViewController: BaseViewController, PaymentMethodDelegate {

    @IBOutlet weak var mTypeMethodSegmentControl: UISegmentedControl!
    @IBOutlet weak var mContentView: UIView!
    
    public var mPaymentMethodPresenter : MethodPaymentPresenter!
    
    override public func viewDidLoad() {
        super.viewDidLoad()
        loadMethodCashPay()
        
        self.mTypeMethodSegmentControl.setTitleTextAttributes([NSForegroundColorAttributeName: UIColor.white], for: .normal)
        
        self.mTypeMethodSegmentControl.setTitleTextAttributes([NSForegroundColorAttributeName: UIColor.white], for: UIControlState.selected)
    }
    
    override public func getPresenter() -> BasePresenter? {
        mPaymentMethodPresenter = MethodPaymentPresenter(viewController: self)
        return mPaymentMethodPresenter
    }
    
    public func loadMethodCashPay(){
        let cashMethodPayView = self.storyboard?.instantiateViewController(withIdentifier: "CashMethodPayViewController") as! CashMethodPayViewController
        cashMethodPayView.view.frame = mContentView.bounds
        cashMethodPayView.mMethodPaymentDelegate = self
        mContentView.addSubview(cashMethodPayView.view)
        addChildViewController(cashMethodPayView)
        cashMethodPayView.didMove(toParentViewController: self)
    }
    
    public func loadMethodCardPay(){
        let cashMethodPayView = self.storyboard?.instantiateViewController(withIdentifier: "CardMethodPayViewController") as! CardMethodPayViewController
        cashMethodPayView.view.frame = mContentView.bounds
        cashMethodPayView.mMethodPaymentDelegate = self
        mContentView.addSubview(cashMethodPayView.view)
        addChildViewController(cashMethodPayView)
        cashMethodPayView.didMove(toParentViewController: self)
    }
    
    public func onSuccessLoadInfoPayment(metodoPagoBean: MetodoPagoBean) {
        mPaymentMethodPresenter.saveMethodPayment(metodoPagoBean: metodoPagoBean)
        resultValue = .RESULT_OK
        ViewControllerUtils.popViewController(viewController: self)
    }

    @IBAction public func mTypeMethodSegmentControl(_ sender: Any) {
        switch mTypeMethodSegmentControl.selectedSegmentIndex {
        case 0:
            loadMethodCashPay()
            break
        case 1:
            loadMethodCardPay()
            break
        default:
            break
        }
    }
    
    override public func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
}
