//
//  ContractingViewController.swift
//  EstrategiaDigital
//
//  Created by Jorge Hdez Villa on 07/08/17.
//  Copyright © 2017 Charls Salazar. All rights reserved.
//

import UIKit
import BaseClases
public class ContractingViewController: BaseEstrategiaViewController, ControllerResultDelegate, SendOpportunityDelegate {
    
    
    
    @IBOutlet weak var mPipeContactView: UIView!
    @IBOutlet weak var mPipePayView: UIView!
    @IBOutlet weak var mPayArrowButton: UIButton!
    @IBOutlet weak var mContactArrowButton: UIButton!
    @IBOutlet weak var mPayImageView: UIImageView!
    @IBOutlet weak var mContactImageView: UIImageView!
    @IBOutlet weak var mTitlePayButton: UIButton!
    @IBOutlet weak var mTitleContactButton: UIButton!
    public static var REQUEST_CODE_ADD_CONTACT_INFO : String = "requestCodeAddContactInfo"
    public static var REQUEST_CODE_ADD_PERSON_INFO : String = "requestCodeAddPersonInfo"
    public static var REQUEST_CODE_ADD_ADDRESS_INFO : String = "requestCodeAddAddressInfo"
    public static var REQUEST_CODE_ADD_DOCUMENTS_INFO : String = "requestCodeAddDocumentsInfo"
    
    public static var REQUEST_CODE_ADD_METHOD_PAYMENT_INFO : String = "requestCodeAddMethodPaymentInfo"

    @IBOutlet weak var mContactContentView: UIView!
    @IBOutlet weak var mPaymentContentView: UIView!

    
    public var mSuccessCaptureContactInfo : Bool = false
    public var mSuccesCapturePaymentInfo : Bool = false
    
    public var mSendOpportunityPresenter : SendOpportunityPresenter!
    
    
    override public func viewDidLoad() {
        super.viewDidLoad()
        
        mContactContentView.layer.borderWidth = 1.0
        mContactContentView.layer.borderColor = UIColor.white.cgColor
        mContactContentView.layer.cornerRadius = 5
        
        mPaymentContentView.layer.borderWidth = 1.0
        mPaymentContentView.layer.borderColor = UIColor.white.cgColor
        mPaymentContentView.layer.cornerRadius = 5
        
        if mFormalityEntity.Prospecto?.Contacto?.Nombre != "" && mFormalityEntity.Prospecto?.Firma?.DireccionFacturacion?.MismaDireccionInstalacion != ""
            && mFormalityEntity.Prospecto?.Firma?.DatosAdicionales?.TipoIdentificacion != "" {
            successCaptureContactoInfo()
        }
        if mFormalityEntity.Prospecto?.Firma?.MetodoPago?.Metodo != "" {
            successCapturePaymentInfo()
        }
    }
    
    override public func getPresenter() -> BasePresenter? {
        mSendOpportunityPresenter = SendOpportunityPresenter(viewController: self, sendOpportunityDelegate: self)
        return mSendOpportunityPresenter
    }
    
    @IBAction public func mContactInfoButton(_ sender: Any) {
        ViewControllerUtils.pushViewControllerWithResult(from: self, to: PersonDataViewController.self, request: ContractingViewController.REQUEST_CODE_ADD_CONTACT_INFO)
    }
    
    public func viewControllerForResult(keyRequest: String, result: ViewControllerResult, data: [String : AnyObject]) {
        if keyRequest == ContractingViewController.REQUEST_CODE_ADD_CONTACT_INFO {
            if result == ViewControllerResult.RESULT_OK {
                successCaptureContactoInfo()
            }
        } else if keyRequest == ContractingViewController.REQUEST_CODE_ADD_METHOD_PAYMENT_INFO {
            if result == ViewControllerResult.RESULT_OK {
                successCapturePaymentInfo()
            }
        }
    }
    
    public func successCaptureContactoInfo(){
        mSuccessCaptureContactInfo = true
        
        mContactImageView.image = mContactImageView.image?.withRenderingMode(.alwaysTemplate)
        mContactImageView.tintColor = UIColor(netHex: Colors.color_grey_select_plan)
        mPipeContactView.backgroundColor = UIColor(netHex: Colors.color_grey_select_plan)
        mTitleContactButton.setTitleColor(UIColor(netHex: Colors.color_grey_select_plan), for: .normal)
        mContactArrowButton.setTitleColor(UIColor(netHex: Colors.color_grey_select_plan), for: .normal)
        
        self.mContactContentView.backgroundColor  = UIColor(netHex:Colors.color_primary_green)
    }
    
    public func successCapturePaymentInfo(){
        mSuccesCapturePaymentInfo = true
        
        mPayImageView.image = mPayImageView.image?.withRenderingMode(.alwaysTemplate)
        mPayImageView.tintColor = UIColor(netHex: Colors.color_grey_select_plan)
        
        mPipePayView.backgroundColor = UIColor(netHex: Colors.color_grey_select_plan)
        
        mTitlePayButton.setTitleColor(UIColor(netHex: Colors.color_grey_select_plan), for: .normal)
        mPayArrowButton.setTitleColor(UIColor(netHex: Colors.color_grey_select_plan), for: .normal)
        
        self.mPaymentContentView.backgroundColor  = UIColor(netHex:Colors.color_primary_green)
    }
    
    @IBAction public func mPaymentButton(_ sender: Any) {
        ViewControllerUtils.pushViewControllerWithResult(from: self, to: PaymentMethodViewController.self, request: ContractingViewController.REQUEST_CODE_ADD_METHOD_PAYMENT_INFO)
    }
    
    @IBAction public func onSendOpportunityClick(_ sender: Any) {
        if mSuccesCapturePaymentInfo && mSuccessCaptureContactInfo {
             mSendOpportunityPresenter.sendOpportunity()
        } else {
            AlertDialog.show(title: "Error", body: "Ingrese toda la información de contacto y pago", view: self)
        }
    }
    
    
    public func onSuccessSendOpportunity() {
        ViewControllerUtils.pushViewController(from: self, to: SyncImagesViewController.self)
    }
    

    override public func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }

}
