//
//  ConfirmLocationViewController.swift
//  EstrategiaDigital
//
//  Created by Charls Salazar on 26/06/17.
//  Copyright © 2017 Charls Salazar. All rights reserved.
//

import UIKit
import BaseClases
public class ConfirmLocationViewController: BaseViewController, ConfirmLocationDelegate {
    
    @IBOutlet weak var zipCodeTextField: UITextField!
    @IBOutlet weak var streetTextField: UITextField!
    @IBOutlet weak var hoodTextField: UITextField!
    @IBOutlet weak var localTextField: UITextField!
    @IBOutlet weak var stateTextField: UITextField!
    @IBOutlet weak var cityTextField: UITextField!
    @IBOutlet weak var numberIntTextField: UITextField!
    @IBOutlet weak var numberExtTextField: UITextField!
    
    public var mFormValidator : FormValidator!
    public var mConfirmLocationPresenter : ConfirmLocationPresenter!
    
    override public func viewDidLoad() {
        super.viewDidLoad()
        mFormValidator = FormValidator(showAllErrors: true)
        mFormValidator.addValidators(validators:
            TextFieldValidator(textField : zipCodeTextField, regex: RegexEnum.NOT_EMPTY, messageError: StringDialogs.dialog_error_empty),
             TextFieldValidator(textField : cityTextField, regex: RegexEnum.NOT_EMPTY, messageError: StringDialogs.dialog_error_empty),
             TextFieldValidator(textField : stateTextField, regex: RegexEnum.NOT_EMPTY, messageError: StringDialogs.dialog_error_empty),
             TextFieldValidator(textField : streetTextField, regex: RegexEnum.NOT_EMPTY, messageError: StringDialogs.dialog_error_empty),
             TextFieldValidator(textField : localTextField, regex: RegexEnum.NOT_EMPTY, messageError: StringDialogs.dialog_error_empty),
             TextFieldValidator(textField : hoodTextField, regex: RegexEnum.NOT_EMPTY, messageError: StringDialogs.dialog_error_empty),
             TextFieldValidator(textField : numberExtTextField, regex: RegexEnum.NOT_EMPTY, messageError: StringDialogs.dialog_error_empty)
        )
        mConfirmLocationPresenter.loadDireccionBean()
    }
    
    override public func getPresenter() -> BasePresenter? {
        mConfirmLocationPresenter = ConfirmLocationPresenter(viewController: self, confirmLocationDelegate: self)
        return mConfirmLocationPresenter
    }
    
    @IBAction public func nextButton(_ sender: Any) {
        if mFormValidator.isValid() {
            let direccionBean : DireccionBean = DireccionBean()
            direccionBean.zipCode = (zipCodeTextField.text?.trim())!
            direccionBean.street = (streetTextField.text?.trim())!
            direccionBean.colony = (hoodTextField.text?.trim())!
            direccionBean.state = (localTextField.text?.trim())!
            direccionBean.city = (cityTextField.text?.trim())!
            direccionBean.noInt = (numberIntTextField.text?.trim())!
            direccionBean.noExt = (numberExtTextField.text?.trim())!
            mConfirmLocationPresenter.confirmDirrecionBean(direccionBean: direccionBean)
        }
    }
    
    public func onSuccessLoadDireccionBean(direccionBean : DireccionBean){
        zipCodeTextField.text = direccionBean.zipCode.isNumber ? direccionBean.zipCode : nil
        streetTextField.text = direccionBean.street.uppercased()
        stateTextField.text = direccionBean.state.uppercased()
        cityTextField.text = direccionBean.city.uppercased()
        hoodTextField.text = direccionBean.colony.uppercased()
        localTextField.text = direccionBean.district.uppercased()
        numberIntTextField.text = direccionBean.noInt
        numberExtTextField.text = direccionBean.noExt
    }
    
    public func onSuccessSaveDireccionBean() {
        ViewControllerUtils.pushViewController(from: self, to: PackagesTypeViewController.self)
    }

    override public func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
}
