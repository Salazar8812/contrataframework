//
//  CashMethodPayViewController.swift
//  EstrategiaDigital
//
//  Created by Charls Salazar on 29/10/17.
//  Copyright © 2017 Charls Salazar. All rights reserved.
//

import UIKit
import BaseClases
public class CashMethodPayViewController: BaseEstrategiaViewController {
    
    @IBOutlet weak var mAmountLabel: UILabel!
    
    @IBOutlet weak var mNameTextField: IconTextField!
    @IBOutlet weak var mLastNameTextField: IconTextField!
    @IBOutlet weak var mSecondLastNameTextField: IconTextField!
    
    public var mFormValidator : FormValidator!
    public var mMethodPaymentDelegate : PaymentMethodDelegate!
    
    @IBOutlet weak var mMountToPayTextField: UILabel!
    override public func viewDidLoad() {
        super.viewDidLoad()
        
        mAmountLabel.text = String(format: "$%.2f", ceil(mFormalityEntity.totalPriceList))
        mFormValidator = FormValidator(showAllErrors: true)
        mFormValidator.addValidators(validators:
            TextFieldValidator(textField: mNameTextField, regex: RegexEnum.NOT_EMPTY),
            TextFieldValidator(textField: mLastNameTextField, regex: RegexEnum.NOT_EMPTY),
            TextFieldValidator(textField: mSecondLastNameTextField, regex: RegexEnum.NOT_EMPTY)
        )
        
        let metodoPagoBean : MetodoPagoBean = (mFormalityEntity.Prospecto?.Firma?.MetodoPago)!
        mNameTextField.text = metodoPagoBean.NombreTitular
        mLastNameTextField.text = metodoPagoBean.ApellidoPaternoTitular
        mSecondLastNameTextField.text = metodoPagoBean.ApellidoMaternoTitular
        
    }

    @IBAction public func mSaveButton(_ sender: Any) {
        if mFormValidator.isValid() {
            let metodoPagoBean : MetodoPagoBean = MetodoPagoBean()
            metodoPagoBean.NombreTitular = (mNameTextField.text?.trim())!
            metodoPagoBean.ApellidoPaternoTitular = (mLastNameTextField.text?.trim())!
            metodoPagoBean.ApellidoMaternoTitular = (mSecondLastNameTextField.text?.trim())!
            metodoPagoBean.Metodo = "Efectivo"
            mMethodPaymentDelegate.onSuccessLoadInfoPayment(metodoPagoBean: metodoPagoBean)
        } else {
            AlertDialog.show(title: "Error", body: StringDialogs.dialog_error_empty_fields_new, view: self)
        }
    }
    
    override public func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
}
