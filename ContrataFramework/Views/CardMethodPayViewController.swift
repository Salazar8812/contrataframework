//
//  CardMethodPayViewController.swift
//  EstrategiaDigital
//
//  Created by Charls Salazar on 29/10/17.
//  Copyright © 2017 Charls Salazar. All rights reserved.
//

import UIKit
import CreditCardValidator
import BaseClases
public class CardMethodPayViewController: BaseViewController, CardIOPaymentViewControllerDelegate{
    
    @IBOutlet weak var mSecondLastNameTextField: IconTextField!
    @IBOutlet weak var mLastNameTextField: IconTextField!
    @IBOutlet weak var mNameTextField: IconTextField!
    @IBOutlet weak var mCardNumberTexField: IconTextField!
    @IBOutlet weak var mDueDateTextField: IconTextField!
    public var mCardInfo: CardIOCreditCardInfo!
    
    @IBOutlet weak var mIconBankImageView: UIImageView!
    @IBOutlet weak var mCardTypeTextField: IconTextField!
    public var mFormValidator : FormValidator!
    public var mMethodPaymentDelegate : PaymentMethodDelegate!
    public let mContactTye : [String] = ["Tarjeta de Credito", "Tarjeta de Débito"]
    public var mTypeIconCard : String?
    
    override public func viewDidLoad() {
        super.viewDidLoad()
        CardIOUtilities.preload()
        mFormValidator = FormValidator(showAllErrors: true)
        mCardTypeTextField.inputView = UIView()
        mCardTypeTextField.setTextPicker(elements: mContactTye)
        
        mFormValidator.addValidators(validators:
            TextFieldValidator(textField : mCardNumberTexField, regex: RegexEnum.NOT_EMPTY, messageError: StringDialogs.dialog_error_empty),
            TextFieldValidator(textField : mCardTypeTextField, regex: RegexEnum.NOT_EMPTY, messageError: StringDialogs.dialog_error_empty),
             TextFieldValidator(textField : mDueDateTextField, regex: RegexEnum.NOT_EMPTY, messageError: StringDialogs.dialog_error_empty),
            TextFieldValidator(textField : mNameTextField, regex: RegexEnum.NOT_EMPTY, messageError: StringDialogs.dialog_error_empty),
            TextFieldValidator(textField : mLastNameTextField, regex: RegexEnum.NOT_EMPTY, messageError: StringDialogs.dialog_error_empty),
            TextFieldValidator(textField : mSecondLastNameTextField, regex: RegexEnum.NOT_EMPTY, messageError: StringDialogs.dialog_error_empty)
        )
    }
    
    override public func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    @IBAction public func mSaveButton(_ sender: Any) {
        if mFormValidator.isValid(){
            let metodoPagoBean : MetodoPagoBean = MetodoPagoBean()
            metodoPagoBean.NombreTitular = (mNameTextField.text?.trim().uppercased())!
            metodoPagoBean.ApellidoPaternoTitular = (mLastNameTextField.text?.trim().uppercased())!
            metodoPagoBean.ApellidoMaternoTitular = (mSecondLastNameTextField.text?.trim().uppercased())!
            if(mCardTypeTextField.text == "Tarjeta de Credito"){
                metodoPagoBean.Metodo = "TDC"
            }else{
                metodoPagoBean.Metodo = "TDD"
            }
            metodoPagoBean.NumeroTarjeta = (mCardNumberTexField.text?.trim())!
            metodoPagoBean.VencimientoMes = "\(mCardInfo.expiryMonth)"
            metodoPagoBean.VencimientoAnio = "\(mCardInfo.expiryYear)"
            metodoPagoBean.TipoTarjeta = mTypeIconCard!
            mMethodPaymentDelegate.onSuccessLoadInfoPayment(metodoPagoBean: metodoPagoBean)
        }else{
            AlertDialog.show(title: "Campos Vacios", body: "Es Necesario llenar todos los campos", view: self)
        }
    }
    
    @IBAction public func mDueDateTextField(_ sender: Any) {
    
    }
    
    
    @IBAction public func mScanCardButton(_ sender: Any) {
        let cardIOVC = CardIOPaymentViewController(paymentDelegate: self)
        cardIOVC?.modalPresentationStyle = .formSheet
        present(cardIOVC!, animated: true, completion: nil)
    }
    
    public func userDidCancel(_ paymentViewController: CardIOPaymentViewController!) {
        paymentViewController?.dismiss(animated: true, completion: nil)
    }
    
    public func userDidProvide(_ cardInfo: CardIOCreditCardInfo!, in paymentViewController: CardIOPaymentViewController!) {
        mCardInfo = cardInfo
        mCardNumberTexField.text = cardInfo.cardNumber
        validateTypeCard(numberCard: cardInfo.cardNumber)
        
        let number = "\(cardInfo.expiryYear)"
        let array = number.characters.flatMap{Int(String($0))} // [1, 2, 3, 4, 5, 6]
        
        mDueDateTextField.text = "\(String(format: "%02d", cardInfo.expiryMonth))/\(array[2])\(array[3])"
        paymentViewController?.dismiss(animated: true, completion: nil)
    }
    
    public func validateTypeCard(numberCard: String){
        let number = numberCard
    
        let v = CreditCardValidator()
        if let type = v.type(from:number) {
            print(type.name) // Visa, Mastercard, Amex etc.
            switch type.name {
            case "Visa":
                mIconBankImageView.image = UIImage(named: "ic_typeVisa")
                mTypeIconCard = ""
                break
            case "MasterCard":
                mIconBankImageView.image = UIImage(named: "ic_typeMaster")
                mTypeIconCard = ""
                break
            case "Amex":
                mIconBankImageView.image = UIImage(named: "ic_typeAmex")
                mTypeIconCard = ""
                break
            default:
                break
            }
            
        } else {
            print("Tarjeta no detectada")
        }
    }
}
