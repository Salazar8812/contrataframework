//
//  PackagesViewController.swift
//  EstrategiaDigital
//
//  Created by Charls Salazar on 05/07/17.
//  Copyright © 2017 Charls Salazar. All rights reserved.
//

import UIKit
import BaseClases
public class PackagesViewController: BaseViewController, FamilyPackageDelegate, CarruselDataDelegate {
    
    @IBOutlet weak var listPackageCarousel: iCarousel!
    
    public var descriptionArray = [DescriptionPackage]()
    public var selectedIndex : Int!
    public var indice : Int!
    public var descriptionPDataSoure : DescriptionPackageDataSource?
    public var mCarruselDataSource : CarruselDataSource?
    public var mFamilyPackagePresenter : FamilyPackagesPresenter?
    @IBOutlet weak var mTitleFamilyPackage: UILabel!
    @IBOutlet weak var mDetailPlanLabel: UILabel!
    public var mArrayFamily: [Plans] = []
    
    override public func viewDidLoad() {
        super.viewDidLoad()
        
        mCarruselDataSource = CarruselDataSource(carrusel: listPackageCarousel, mCarruselDataDelegate: self)
        mCarruselDataSource?.settings()
        
        mFamilyPackagePresenter?.loadFamilyPackage()
    }
    
    override public func getPresenter() -> BasePresenter? {
        mFamilyPackagePresenter = FamilyPackagesPresenter(viewController: self, mFamilyPackageDelegate: self)
        return mFamilyPackagePresenter
    }
    
    override public func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    
    public func onSuccessFamilyPackage(familyPackageResponse: [Plans]) {
        if familyPackageResponse.count > 0 {
            mCarruselDataSource?.update(items:familyPackageResponse)
            mArrayFamily = familyPackageResponse
            mTitleFamilyPackage.text = mArrayFamily[0].planName
            mDetailPlanLabel.text = mArrayFamily[0].planDetail
        }
    }
    
    public func onItemClick(planSelected: Plans) {
        var extras : [String : AnyObject] = [:]
        extras[KeysEnum.EXTRA_PLAN] = planSelected
        ViewControllerUtils.pushViewController(from: self, to: AdditionalServiceViewController.self, extras: extras)
    }
    
    public func onChangeItem(planDetail: String, planName: String) {
        mTitleFamilyPackage.text = planName
        mDetailPlanLabel.text = planDetail
    }
    
}

