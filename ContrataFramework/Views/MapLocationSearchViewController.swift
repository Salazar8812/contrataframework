//
//  MapLocationSearchViewController.swift
//  EstrategiaDigital
//
//  Created by Charls Salazar on 26/06/17.
//  Copyright © 2017 Charls Salazar. All rights reserved.
//

import UIKit
import GoogleMaps
import CoreLocation
import BaseClases
public class MapLocationSearchViewController: BaseViewController, PredictionAddressDelegate, TableViewCellClickDelegate, MapLocationSearchDelegate,CLLocationManagerDelegate,GMSMapViewDelegate, GeocoderDelegate {
    
    @IBOutlet weak var mGoogleMapView: GMSMapView!
    @IBOutlet weak var searchContainerView: UIView!
    @IBOutlet weak var serchTextField: UITextField!
    
    @IBOutlet weak var resulTableView: UITableView!
    
    @IBOutlet weak var nextContinueButton: UIButton!
    @IBOutlet weak var mNotCoverageView: UIView!
    
    public var autoCompleteDirection: [String] = []
    public var autoComplete: [String] = []
    public var addrressComplete : [AddressComponent] = []
    public var mUserPosition : CLLocationCoordinate2D!
    
    @IBOutlet weak var mColonyTextField: IconTextField!
    @IBOutlet weak var mZipCodeTextField: IconTextField!
    @IBOutlet weak var mPhoneTextField: IconTextField!
    @IBOutlet weak var mEmailTextField: IconTextField!
    @IBOutlet weak var mNameTextField: IconTextField!
    public var mMapLocationSearchPresenter : MapLocationSearchPresenter!
    public var predictionPresenter : PredictionAddressPresenter!
    public var mGeocodePresenter : GeocoderPresenter!
    public var mAddressDataSource : BaseDataSource<PredictionAddress, CellDirectionTableViewCell>!
    public var locationManager: CLLocationManager!

    public var lat: Double?
    public var lon: Double?
    public let marker = GMSMarker()
    public var firstCoordinates : Bool?
    public var mDireccionBean : DireccionBean!
    
    public var mFormValidator : FormValidator!
    public var mNoCoveragePresenter : NoCoveragePresenter?
    
    public var placeHolder = NSMutableAttributedString()
    
    public let mNamePlaceHolder = "Nombre completo"
    public let mEmailPlaceHolder = "Correo"
    public let mPhonePlaceHolder = "Teléfono"
    public let mZipCodePlaceHolder = "Código Postal"
    public let mColonyPlaceHolder = "Colonia"

    
    override public func viewDidLoad() {
        super.viewDidLoad()
        mFormValidator = FormValidator(showAllErrors: true)
        serchTextField.delegate = self
        firstCoordinates = true
        
        ViewUtils.setVisibility(view: resulTableView, visibility: .GONE)
        ViewUtils.setVisibility(view: mNotCoverageView, visibility: .GONE)
        
        mFormValidator.addValidators(validators:
            TextFieldValidator(textField : mZipCodeTextField, regex: RegexEnum.NOT_EMPTY, messageError: StringDialogs.dialog_error_empty),
             TextFieldValidator(textField : mColonyTextField, regex: RegexEnum.NOT_EMPTY, messageError: StringDialogs.dialog_error_empty),
             TextFieldValidator(textField: mPhoneTextField, minCharacters: 10, maxCharacters: 10, messageError: ""),
             TextFieldValidator(textField : mNameTextField, regex: RegexEnum.NOT_EMPTY, messageError: StringDialogs.dialog_error_empty),
             TextFieldValidator(textField: mEmailTextField, regex: RegexEnum.EMAIL)        )
        
        mAddressDataSource = BaseDataSource(tableView: resulTableView, delegate: self)
    }
    
    override public func viewWillAppear(_ animated: Bool) {
        getLocation()
    }
    public func getLocation(){
        locationManager = CLLocationManager()
        locationManager?.delegate = self
        locationManager?.requestWhenInUseAuthorization()
        locationManager?.startUpdatingLocation()
    }
    
    public func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        let substring = (textField.text! as NSString).replacingCharacters(in: range, with: string)
        if(substring.characters.count > 4 ){
            searchAutocompleteDirection(substring)
        }
        return true
    }
    
    override public func getPresenters() -> [BasePresenter]? {
        mMapLocationSearchPresenter = MapLocationSearchPresenter(viewController: self, mapLocationSearchDelegate: self)
        mNoCoveragePresenter = NoCoveragePresenter(viewController: self)
        predictionPresenter = PredictionAddressPresenter(delegate: self)
        mGeocodePresenter = GeocoderPresenter(viewController: self, geocoderDelegate : self)
        return [mMapLocationSearchPresenter, predictionPresenter, mGeocodePresenter]
    }
    
    public func onErrorCoverageValidate(){
        ViewUtils.setVisibility(view: nextContinueButton, visibility: .GONE, controller : self, animated : true)
        ViewUtils.setVisibility(view: searchContainerView, visibility: .GONE, controller : self, animated : true)
        ViewUtils.setVisibility(view: mNotCoverageView, visibility: .NOT_GONE, controller : self, animated : true)
    }
    
    override public func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    
    public func searchAutocompleteDirection(_ substring: String) {
        autoComplete.removeAll(keepingCapacity: false)
        print(substring)
        predictionPresenter?.getPredictionAddress(keyword: substring as NSString)
    }
    
    public func onSuccessPrediction(collectionAddress: [PredictionAddress]) {
        ViewUtils.setVisibility(view: resulTableView, visibility: .NOT_GONE)
        mAddressDataSource.update(items: collectionAddress)
    }
    
    public func onSendingPrediction(){
    }
    
    public func onErrorPrediction(errorMessage: String) {
       // AlertDialog.show(title: "Aviso", body: errorMessage, view: self)
    }
    
    @IBAction public func onHideAddressButtonClick(_ sender: Any) {
        ViewUtils.setVisibility(view: resulTableView, visibility: .GONE)
    }
    
    public func onSendingGeometry(){
    }
    
    public func onErrorGeometry(errorMessage : String){
    }
    
    public func onErrorConnectionGeometry(){
        
    }
    
    public func onErrorConnection(){
    }
    
    public func onSuccessGeometry(geoLatLon: Geometry, formatAddress: [AddressComponent]) {
        addrressComplete = formatAddress
        let camera = GMSCameraPosition.camera(withLatitude: (geoLatLon.location?.lat)!, longitude: (geoLatLon.location?.lng)!, zoom: 17.0)
        mGoogleMapView.animate(to: camera)
        
        mUserPosition = CLLocationCoordinate2D(latitude: (geoLatLon.location?.lat)!, longitude: (geoLatLon.location?.lng)!)
        marker.position = mUserPosition
        marker.icon = UIImage(named: "ic_google_pin_empty")
        marker.map = mGoogleMapView
        
        ViewUtils.setVisibility(view: self.resulTableView, visibility: .GONE)
    }
    
    public func onTableViewCellClick(item: NSObject, cell: UITableViewCell) {
        ViewUtils.setVisibility(view: resulTableView, visibility: .GONE)
        serchTextField.resignFirstResponder()
        let predictionAddress : PredictionAddress = item as! PredictionAddress
        predictionPresenter?.getLatLon(placeId: predictionAddress.place_id!)
        serchTextField.text = predictionAddress.description_!
    }

    public func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        if(firstCoordinates)!{
            lat = locations[0].coordinate.latitude
            lon = locations[0].coordinate.longitude
            firstCoordinates = false
            
            let camera = GMSCameraPosition.camera(withLatitude: lat!, longitude: lon!, zoom: 17.0)
            mGoogleMapView.animate(to: camera)
            
            mUserPosition = CLLocationCoordinate2D(latitude: lat!, longitude: lon!)
            marker.position = mUserPosition
            marker.title = "Charls House"
            marker.snippet = "México"
            marker.icon = UIImage(named: "ic_google_pin_empty")
            marker.map = mGoogleMapView
        }
    }
    
    @IBAction public func nextContinueButton(_ sender: Any) {
        mMapLocationSearchPresenter.validateCoverage(position: mGoogleMapView.camera.target)
    }
    
    public func onSuccessLoadFactibility(direccionBean : DireccionBean){
        mDireccionBean = direccionBean
        mGeocodePresenter.getAddressForLatLng(latitude: String(mGoogleMapView.camera.target.latitude), longitude: String(mGoogleMapView.camera.target.longitude))
    }
    
    public func onSuccessLoadAddress(dirrecionBean: DireccionBean) {
        mDireccionBean.noExt = dirrecionBean.noExt
        mDireccionBean.street = dirrecionBean.street
        mDireccionBean.city = dirrecionBean.city
        mDireccionBean.state = dirrecionBean.state
        mDireccionBean.delegation = dirrecionBean.delegation
        mDireccionBean.colony = dirrecionBean.colony
        mDireccionBean.zipCode = dirrecionBean.zipCode
        mZipCodeTextField.text = dirrecionBean.zipCode
        RoutingApp.OnConfirmLocation(viewController: self, direccionBean : mDireccionBean)
    }
    
    @IBAction public func onCancelSendNotCoverageClick(_ sender: Any) {
        ViewUtils.setVisibility(view: nextContinueButton, visibility: .NOT_GONE, controller : self, animated : true)
        ViewUtils.setVisibility(view: searchContainerView, visibility: .NOT_GONE, controller : self, animated : true)
        ViewUtils.setVisibility(view: mNotCoverageView, visibility: .GONE, controller : self, animated : true)
    }
    
    @IBAction public func onSendNotCoverageClick(_ sender: Any) {
        if mFormValidator.isValid() {
            mNoCoveragePresenter?.NoCoverage(name: mNameTextField.text!, email: mEmailTextField.text!, phone: mPhoneTextField.text!, zipCode: mZipCodeTextField.text!, colony: mColonyTextField.text!)
            ViewUtils.setVisibility(view: nextContinueButton, visibility: .NOT_GONE, controller : self, animated : true)
            ViewUtils.setVisibility(view: searchContainerView, visibility: .NOT_GONE, controller : self, animated : true)
            ViewUtils.setVisibility(view: mNotCoverageView, visibility: .GONE, controller : self, animated : true)
        }
    }

}
