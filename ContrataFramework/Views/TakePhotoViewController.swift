//
//  TakePhotoViewController.swift
//  EstrategiaDigital
//
//  Created by Charls Salazar on 27/10/17.
//  Copyright © 2017 Charls Salazar. All rights reserved.
//

import UIKit
import BaseClases

public class TakePhotoViewController: BaseEstrategiaViewController,ControllerResultDelegate {
    
    public static var REQUEST_CODE_PHOTO_FRONT_SIDE: String = "requestCodePhotoFrontSide"
    public static var REQUEST_CODE_PHOTO_BEHIN_SIDE: String = "requestCodePhotoBehindSide"
    public static var REQUEST_CODE_PHOTO_ADDRESS_VOUCHER : String = "requestCodeAddressVoucher"

    @IBOutlet weak var mIdentificationTypeTextField: IconTextField!
    @IBOutlet weak var mIdentificationNumberTextField: IconTextField!
    
    @IBOutlet weak var mFrontIdentificationImageView: UIImageView!
    @IBOutlet weak var mBackIdentificationImageView: UIImageView!
    @IBOutlet weak var mVoucherImageView: UIImageView!
    
    @IBOutlet weak var mIdentificationAsVoucherCheckBoxButton: CheckBoxButton!
    @IBOutlet weak var mVoucherContentView: UIView!
    
    public var mImages : [UIImage?] = [nil, nil, nil]
    public let mIdentificationTypes : [String] = ["IFE", "Licencia", "Pasaporte", "Cédula profesional", "FMM", "INAPAM"]
    public var mTakePhotoPresenter : TakePhotoPresenter!
    public var mFormValidator : FormValidator!
    public var isSelectedVoucherAddress : Bool = false
    public var isPhotoOne : Bool = false
    public var isPhotoTwo : Bool = false
    public var isPhotoThree : Bool = false
    
    override public func viewDidLoad() {
        super.viewDidLoad()
        
        mIdentificationTypeTextField.setTextPicker(elements: mIdentificationTypes)
        
        mIdentificationTypeTextField.text = mFormalityEntity.Prospecto?.Firma?.DatosAdicionales?.TipoIdentificacion
        mIdentificationNumberTextField.text = mFormalityEntity.Prospecto?.Firma?.DatosAdicionales?.IdentificacionOficial
        ViewUtils.setVisibility(view: mVoucherContentView, visibility: .GONE)
        mIdentificationAsVoucherCheckBoxButton.isChecked = true
        
        mFrontIdentificationImageView.layer.borderWidth = 1.0
        mFrontIdentificationImageView.layer.borderColor = UIColor.white.cgColor
        mFrontIdentificationImageView.layer.cornerRadius = 10
        
        mBackIdentificationImageView.layer.borderWidth = 1.0
        mBackIdentificationImageView.layer.borderColor = UIColor.white.cgColor
        mBackIdentificationImageView.layer.cornerRadius = 10
        
        mVoucherImageView.layer.borderWidth = 1.0
        mVoucherImageView.layer.borderColor = UIColor.white.cgColor
        mVoucherImageView.layer.cornerRadius = 5
        
        mFormValidator = FormValidator(showAllErrors: true)
        mFormValidator.addValidators(validators:
            TextFieldValidator(textField: mIdentificationTypeTextField, regex: RegexEnum.NOT_EMPTY),
            TextFieldValidator(textField: mIdentificationNumberTextField, regex: RegexEnum.NOT_EMPTY)
        )
        
        /*isPhotoOne = loadImageCaptured(image: mFrontIdentificationImageView, identifier: "frontImage.png")
        isPhotoTwo = loadImageCaptured(image: mBackIdentificationImageView, identifier: "backImage.png")
        isPhotoThree = loadImageCaptured(image: mVoucherImageView, identifier: "voucherImage.png")
        
        saveDataInArray()*/
    }
    
    /*func saveDataInArray(){
        if(isPhotoOne){
            mImages[0] = mFrontIdentificationImageView.image
        }else if(isPhotoTwo){
            mImages[1] = mFrontIdentificationImageView.image
        }else if(isPhotoThree){
            mImages[2] = mFrontIdentificationImageView.image
        }
    }*/
    
   /* func loadImageCaptured(image : UIImageView , identifier : String)->Bool{
        var isActivaded = false
        
        let nsDocumentDirectory = FileManager.SearchPathDirectory.documentDirectory
        let nsUserDomainMask    = FileManager.SearchPathDomainMask.userDomainMask
        let paths               = NSSearchPathForDirectoriesInDomains(nsDocumentDirectory, nsUserDomainMask, true)
        if let dirPath          = paths.first
        {
            let imageURL = URL(fileURLWithPath: dirPath).appendingPathComponent(identifier)
            image.image = UIImage(contentsOfFile: imageURL.path)
            if(imageURL.path != ""){
                isActivaded = true
            }else{
                isActivaded = false
            }
        }
        
        return isActivaded
    }*/
    
    override public func getPresenter() -> BasePresenter? {
        mTakePhotoPresenter = TakePhotoPresenter(viewController: self)
        return mTakePhotoPresenter
    }
    
    @IBAction public func onVoucherCheckButtonClick(_ sender: Any) {
        if mIdentificationAsVoucherCheckBoxButton.isChecked {
            isSelectedVoucherAddress = false
            ViewUtils.setVisibility(view: mVoucherContentView, visibility: .GONE, controller: self, animated: true)
        } else {
            isSelectedVoucherAddress = true
            ViewUtils.setVisibility(view: mVoucherContentView, visibility: .NOT_GONE, controller: self, animated: true)
        }
    }
    
    @IBAction public func onSaveClick(_ sender: Any) {
        if mFormValidator.isValid() {
            if(isPhotoOne && isPhotoTwo){
                if(isSelectedVoucherAddress){
                    if(isPhotoThree){
                       SaveImages()
                    }else{
                        AlertDialog.show(title: "Advertencia", body: "Es necesario Agregar todas las fotos", view: self)
                    }
                }else{
                    SaveImages()
                }
            }else{
                AlertDialog.show(title: "Advertencia", body: "Es necesario Agregar todas las fotos", view: self)
            }
        }
    }
    
    
    public func SaveImages(){
        mTakePhotoPresenter.saveImages(images: mImages)
        resultValue = .RESULT_OK
        resultDelegate?.viewControllerForResult(keyRequest: requestValue, result: resultValue, data: data)
        self.navigationController?.popToViewController((self.navigationController?.viewControllers[(self.navigationController?.viewControllers.count)! - 4])!, animated: true)
    }
    
    @IBAction public func mIdFrontSideButton(_ sender: Any) {
        ViewControllerUtils.pushViewControllerWithResult(from: self, to: CapturePhotoViewController.self, request: TakePhotoViewController.REQUEST_CODE_PHOTO_FRONT_SIDE)
    }
    
    @IBAction public func mIdBehindSideButton(_ sender: Any) {
        ViewControllerUtils.pushViewControllerWithResult(from: self, to: CapturePhotoViewController.self, request: TakePhotoViewController.REQUEST_CODE_PHOTO_BEHIN_SIDE)
    }
    
    @IBAction public func mAddressVoucherButton(_ sender: Any) {
        ViewControllerUtils.pushViewControllerWithResult(from: self, to: CapturePhotoViewController.self, request: TakePhotoViewController.REQUEST_CODE_PHOTO_ADDRESS_VOUCHER)
    }
    
    public func viewControllerForResult(keyRequest: String, result: ViewControllerResult, data: [String : AnyObject]) {
        switch keyRequest {
        case TakePhotoViewController.REQUEST_CODE_PHOTO_FRONT_SIDE:
            if result == ViewControllerResult.RESULT_OK {
                resultValue = .RESULT_OK
                mImages[0] = data[KeysEnum.EXTRA_CAPTURE_IMAGE] as? UIImage
                mFrontIdentificationImageView.image = data[KeysEnum.EXTRA_CAPTURE_IMAGE] as? UIImage
                isPhotoOne = true
                /*if let data = UIImagePNGRepresentation((data[KeysEnum.EXTRA_CAPTURE_IMAGE] as? UIImage)!) {
                    let fm = FileManager.default
                    let docsurl = try! fm.url(for:.documentDirectory, in: .userDomainMask, appropriateFor: nil, create: false)
                    let filename = docsurl.appendingPathComponent("frontImage.png")
                    try? data.write(to: filename)
                }*/
            }
            break
            
        case TakePhotoViewController.REQUEST_CODE_PHOTO_BEHIN_SIDE:
            if result == ViewControllerResult.RESULT_OK {
                resultValue = .RESULT_OK
                mImages[1] = data[KeysEnum.EXTRA_CAPTURE_IMAGE] as? UIImage
                mBackIdentificationImageView.image = data[KeysEnum.EXTRA_CAPTURE_IMAGE] as? UIImage
                isPhotoTwo = true
                /*if let data = UIImagePNGRepresentation((data[KeysEnum.EXTRA_CAPTURE_IMAGE] as? UIImage)!) {
                    let fm = FileManager.default
                    let docsurl = try! fm.url(for:.documentDirectory, in: .userDomainMask, appropriateFor: nil, create: false)
                    let filename = docsurl.appendingPathComponent("backImage.png")
                    try? data.write(to: filename)
                }*/
            }
            break
            
        case TakePhotoViewController.REQUEST_CODE_PHOTO_ADDRESS_VOUCHER:
            if result == ViewControllerResult.RESULT_OK {
                resultValue = .RESULT_OK
                mImages[2] = data[KeysEnum.EXTRA_CAPTURE_IMAGE] as? UIImage
                mVoucherImageView.image = data[KeysEnum.EXTRA_CAPTURE_IMAGE] as? UIImage
                isPhotoThree = true
                /*if let data = UIImagePNGRepresentation((data[KeysEnum.EXTRA_CAPTURE_IMAGE] as? UIImage)!) {
                    let fm = FileManager.default
                    let docsurl = try! fm.url(for:.documentDirectory, in: .userDomainMask, appropriateFor: nil, create: false)
                    let filename = docsurl.appendingPathComponent("voucher.png")
                    try? data.write(to: filename)
                }*/
            }
            break
            
        default:
            break
        }
    
    }
    
    override public func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }

    
    
}
