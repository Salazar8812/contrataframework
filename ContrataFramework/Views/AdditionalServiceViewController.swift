//
//  AdditionalServiceViewController.swift
//  EstrategiaDigital
//
//  Created by Charls Salazar on 18/07/17.
//  Copyright © 2017 Charls Salazar. All rights reserved.
//

import UIKit
import moa
import BaseClases

public class AdditionalServiceViewController: BaseViewController, AddonsSelectorDelegate, AddonsCarouselDelegate {
    
    @IBOutlet weak var pagerAdditionalServices: iCarousel!
    @IBOutlet weak var mNamePlanLabel: UILabel!
    @IBOutlet weak var mDescriptionAddons: UILabel!
    @IBOutlet weak var mPricePlanLabel: UILabel!
    @IBOutlet weak var mPriceSoonPaymentLabel: UILabel!
    
    public var mPriceConverted : Double = 0
    public var mAddonSelectorPresenter : AddonSelectorPresenter?
    
    public var addonsCarouselDataSource : AddonsCarouselDataSource!
    
    override public func viewDidLoad() {
        super.viewDidLoad()
        
        addonsCarouselDataSource = AddonsCarouselDataSource(carrusel : pagerAdditionalServices, addonsCarouselDelegate: self)
        addonsCarouselDataSource.settings()
        
        clearData()
        mAddonSelectorPresenter?.loadPlan()
        mAddonSelectorPresenter?.loadAddons()
    }
    
    public func clearData() {
        mNamePlanLabel.text = " "
        mDescriptionAddons.text = " "
        mPricePlanLabel.text = " "
        mPriceSoonPaymentLabel.text = " "
    }

    public func onAddNewAddonSelect(adicionalsSelected : [AdicionalesBean]){
        var descriptionAddons = " "
        for adicionalBean in adicionalsSelected {
            descriptionAddons.append(" + \(adicionalBean.getAdicionalName())")
        }
        mDescriptionAddons.text = descriptionAddons
        mAddonSelectorPresenter?.generateAmountPrice(adicionalsSelected: adicionalsSelected)
    }
    
    public func onCalculateTotalAmount(priceList: Double, priceSoonPayment: Double) {
        mPricePlanLabel.text = "Precio de lista: $ \(String(format: "%.2f", ceil(priceList)))"
        mPriceSoonPaymentLabel.text = "Precio de pronto pago: $ \(String(format: "%.2f", ceil(priceSoonPayment)))"
    }
    
    override public func getPresenter() -> BasePresenter? {
        mAddonSelectorPresenter = AddonSelectorPresenter(viewController: self, mAddonSelectorDelegate: self)
        return mAddonSelectorPresenter
    }
    
    @IBAction public func onNextButtonClick(_ sender: Any) {
        mAddonSelectorPresenter?.onSeelectAdicionals(mAdicionalsSelected: addonsCarouselDataSource.mAdicionalsSelected)
    }
    
    public func onSuccessLoadPlan(plan: Plans) {
        mNamePlanLabel.text = plan.planName
    }
    
    public func onCalculateTotalAmount(price: Double) {
        mPricePlanLabel.text = "$\(String(format: "%.2f", price))"
    }
    
    public func onSuccessAddons(adicionalesBean: [AdicionalesBean]) {
        addonsCarouselDataSource.update(items: adicionalesBean)
        mAddonSelectorPresenter?.generateAmountPrice(adicionalsSelected: [])
    }
    
    public func onSuccessSaveProposalBean() {
        ViewControllerUtils.pushViewController(from: self, to: ContractingViewController.self)
    }
    override public func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
}

