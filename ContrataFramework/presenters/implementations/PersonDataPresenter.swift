//
//  PersonDataPresenter.swift
//  EstrategiaDigital
//
//  Created by Jorge Hdez VIlla on 29/10/17.
//  Copyright © 2017 Charls Salazar. All rights reserved.
//

import UIKit
import BaseClases
public class PersonDataPresenter: BaseEstrategiaPresenter {
    
    public func saveContactoBean(contactoBean : ContactoBean, infoAdicionalBean : DatosAdicionalesBean){
        mDataManager.tx(execute: { (tx) -> Void in
            mFormalityEntity.Prospecto?.Contacto = contactoBean
            mFormalityEntity.Prospecto?.Firma?.DatosAdicionales = infoAdicionalBean
            tx.save(object: mFormalityEntity)
        })
    }

}
