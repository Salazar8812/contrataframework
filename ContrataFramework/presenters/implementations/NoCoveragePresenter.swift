//
//  NoCoveragePresenter.swift
//  EstrategiaDigital
//
//  Created by Charls Salazar on 30/10/17.
//  Copyright © 2017 Charls Salazar. All rights reserved.
//

import UIKit
import BaseClases
public protocol NoCoverageDelegate : NSObjectProtocol{
    func onSuccessSendNoCoverage()
}

public class NoCoveragePresenter: BaseEstrategiaPresenter {
    
    public var mNoCoverageDelegate: NoCoverageDelegate?
    
    public func NoCoverage(name: String, email:String, phone: String, zipCode: String, colony:String){
        let clienData = ClientData()
        clienData.name = name
        clienData.email = email
        clienData.phone = phone
        clienData.colony = colony
        clienData.zipCode = zipCode
        let mNoCoverageRequest : NoCoverageRequest = NoCoverageRequest(clientData: clienData)
        RetrofitManager<BaseResponse>.init(requestUrl: ApiDefinition.WS_NO_COVERAGE, delegate: self).request(requestModel: mNoCoverageRequest)
    }
    
    override public func onRequestWs() {
        AlertDialog.showOverlay()
    }
    
    override public func onSuccessLoadResponse(requestUrl: String, response: BaseResponse) {
        AlertDialog.hideOverlay()
        if requestUrl == ApiDefinition.WS_NO_COVERAGE {
            if (response.resultObj?.idResult == "1"){
                AlertDialog.show(title: "DATOS ENVIADOS", body: (response.resultObj?.descriptionValue)!, view: mViewController, handler: { (action: UIAlertAction!) in
                    ViewControllerUtils.popViewController(viewController: self.mViewController)
                })
            } else {
                onErrorLoadResponse(requestUrl: requestUrl, messageError: "");
            }
        }
    }
    
    public func onSuccessSendNoCoverage(){
        
    }
}
