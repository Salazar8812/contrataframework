//
//  PackagesTypePresenter.swift
//  EstrategiaDigital
//
//  Created by Jorge Hdez VIlla on 19/09/17.
//  Copyright © 2017 Charls Salazar. All rights reserved.
//

import UIKit
import BaseClases
public protocol PackageTypeDelegate: NSObjectProtocol{
    func onSuccessPMPlanes(arrFamily: [ArrFamily])
}

public class PackagesTypePresenter: BaseEstrategiaPresenter {

    public var mSelectedType : String = ""
    public var mPackageTypeDelegate : PackageTypeDelegate!
    
    public init(viewController: BaseViewController, mPackageTypeDelegate : PackageTypeDelegate){
        super.init(viewController: viewController)
        self.mPackageTypeDelegate = mPackageTypeDelegate
    }
    
    public func loadHomePackages(){
        mSelectedType = PackagesType.RESIDENTIAL;
        let requestModel : ProductMasterPlainsRequest = ProductMasterPlainsRequest(information: Information(type: mSelectedType))
        RetrofitManager<ProductMasterPlanesResponse>.init(requestUrl: ApiDefinition.WS_GET_PLANS, delegate: self).request(requestModel: requestModel)
    }
    
    public func loadBusinessPackages(){
        mSelectedType = PackagesType.BUSINESS
        let requestModel : ProductMasterPlainsRequest = ProductMasterPlainsRequest(information: Information(type: mSelectedType))
        RetrofitManager<ProductMasterPlanesResponse>.init(requestUrl: ApiDefinition.WS_GET_PLANS, delegate: self).request(requestModel: requestModel)
    }

    override public func onSuccessLoadResponse(requestUrl: String, response: BaseResponse) {
        if (requestUrl == ApiDefinition.WS_GET_PLANS){
            onSuccessPMPlanes(producMaster: response as! ProductMasterPlanesResponse)
        }
        AlertDialog.hideOverlay()
    }
    
    public func onSuccessPMPlanes(producMaster: ProductMasterPlanesResponse){
        mPackageTypeDelegate.onSuccessPMPlanes(arrFamily: producMaster.arrFamily)
    }
}
