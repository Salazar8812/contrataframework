//
//  FamilyPackagesPresenter.swift
//  EstrategiaDigital
//
//  Created by Charls Salazar on 23/10/17.
//  Copyright © 2017 Charls Salazar. All rights reserved.
//

import UIKit
import BaseClases

public protocol FamilyPackageDelegate : NSObjectProtocol {
    func onSuccessFamilyPackage(familyPackageResponse : [Plans])
}

public class FamilyPackagesPresenter: BaseEstrategiaPresenter {
    
    public var mArrFamily : ArrFamily!
    public var mFamilyPackageDelegate : FamilyPackageDelegate!
    public var midPlans : [IDPlan] = []
    
    public init(viewController: BaseViewController, mFamilyPackageDelegate : FamilyPackageDelegate){
        super.init(viewController: viewController)
        self.mFamilyPackageDelegate = mFamilyPackageDelegate
    }
    
    override public func viewDidLoad(){
        if mViewController.hasExtra(key: KeysEnum.EXTRA_ARR_FAMILY) {
            mArrFamily = mViewController.extras[KeysEnum.EXTRA_ARR_FAMILY] as! ArrFamily
        }
    }
    
    public func loadFamilyPackage(){
        var idPlans : [IDPlan] = []
        
        for item in mArrFamily.arrPlan {
                idPlans.append(IDPlan(idPlan: item.planId))
        }
   
        let requestModel : FamilyPackageRequest = FamilyPackageRequest(idPlans: idPlans)
        RetrofitManager<FamilyPackageResponse>.init(requestUrl: ApiDefinition.WS_FAMILY_PACKAGE, delegate: self).request(requestModel: requestModel)
    }
    
    override public func onSuccessLoadResponse(requestUrl: String, response: BaseResponse) {
        if (requestUrl == ApiDefinition.WS_FAMILY_PACKAGE){
            onSuccessFamilyPackage(requestUrl: requestUrl, familyPackageResponse: response as! FamilyPackageResponse)
        }
        AlertDialog.hideOverlay()
    }
    
    public func onSuccessFamilyPackage(requestUrl : String, familyPackageResponse: FamilyPackageResponse){
        if familyPackageResponse.result == "0" {
            mFamilyPackageDelegate.onSuccessFamilyPackage(familyPackageResponse: familyPackageResponse.plans)
        } else {
            super.onErrorLoadResponse(requestUrl: requestUrl, messageError: familyPackageResponse.resultDescription)
        }
    }
    
}
