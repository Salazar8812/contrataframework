//
//  AddressDataPresenter.swift
//  EstrategiaDigital
//
//  Created by Jorge Hdez VIlla on 29/10/17.
//  Copyright © 2017 Charls Salazar. All rights reserved.
//

import UIKit
import BaseClases

public class AddressDataPresenter: BaseEstrategiaPresenter {
    
    public func saveDireccionFacturacionBean(){
        let direccionFacturacion : DireccionFacturacionBean = DireccionFacturacionBean()
        let direccionBean : DireccionBean = (mFormalityEntity.Prospecto?.Direccion)!
        direccionFacturacion.CodigoPostal = direccionBean.zipCode
        direccionFacturacion.Calle = direccionBean.street
        direccionFacturacion.NumeroExterior = direccionBean.noExt
        direccionFacturacion.NumeroInterior = direccionBean.noInt
        direccionFacturacion.Colonia = direccionBean.colony
        direccionFacturacion.Ciudad = direccionBean.city
        direccionFacturacion.DelegacionMunicipio = direccionBean.delegation
        direccionFacturacion.Estado = direccionBean.state
        direccionFacturacion.MismaDireccionInstalacion = "true"
        mDataManager.tx(execute: { (tx) -> Void in
            mFormalityEntity.Prospecto?.Firma?.DireccionFacturacion = direccionFacturacion
            tx.save(object: mFormalityEntity)
        })
    }

    public func saveDirecionFacturacionBean(direccionFacturacionBean : DireccionFacturacionBean){
        direccionFacturacionBean.MismaDireccionInstalacion = "false"
        mDataManager.tx(execute: { (tx) -> Void in
            mFormalityEntity.Prospecto?.Firma?.DireccionFacturacion = direccionFacturacionBean
            tx.save(object: mFormalityEntity)
        })
    }
    
}
