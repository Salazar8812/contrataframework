//
//  SendOpportunityPresenter.swift
//  EstrategiaDigital
//
//  Created by Jorge Hdez VIlla on 30/10/17.
//  Copyright © 2017 Charls Salazar. All rights reserved.
//

import UIKit
import BaseClases
public protocol SendOpportunityDelegate : NSObjectProtocol {
    func onSuccessSendOpportunity()
}

public class SendOpportunityPresenter: BaseEstrategiaPresenter {
    
    public var mSendOpportunityDelegate : SendOpportunityDelegate!
    
    public init(viewController: BaseViewController, sendOpportunityDelegate : SendOpportunityDelegate) {
        super.init(viewController: viewController)
        self.mSendOpportunityDelegate = sendOpportunityDelegate
    }
    
    public func sendOpportunity(){
        if mFormalityEntity.Prospecto?.Contacto?.Nombre != "" && mFormalityEntity.Prospecto?.Vendedor?.IdEmpleado != "" && mFormalityEntity.Prospecto?.Firma?.MetodoPago?.Metodo != "" {
            let formalityEntity : FormalityEntity = FormalityEntity()
            formalityEntity.Prospecto = ProspectoBean()
            formalityEntity.Prospecto?.Contacto = generateContacto(contactoBean: (mFormalityEntity.Prospecto?.Contacto)!)
            formalityEntity.Prospecto?.Direccion = mFormalityEntity.Prospecto?.Direccion
            formalityEntity.Prospecto?.Vendedor = mFormalityEntity.Prospecto?.Vendedor
            formalityEntity.Prospecto?.Propuesta = PropuestaBean()
            formalityEntity.Prospecto?.Propuesta = mFormalityEntity.Prospecto?.Propuesta
            formalityEntity.Prospecto?.Firma = FirmaBean()
            formalityEntity.Prospecto?.Firma?.DatosAdicionales = mFormalityEntity.Prospecto?.Firma?.DatosAdicionales
            formalityEntity.Prospecto?.Firma?.DireccionFacturacion = mFormalityEntity.Prospecto?.Firma?.DireccionFacturacion
            formalityEntity.Prospecto?.Firma?.Referencias = mFormalityEntity.Prospecto?.Firma?.Referencias
            formalityEntity.Prospecto?.Firma?.MetodoPago = generateMetodoPago(metodoPagoBean: (mFormalityEntity.Prospecto?.Firma?.MetodoPago)!)
            
            RetrofitManager<OpportunityResponse>.init(requestUrl: ApiDefinition.WS_SEND_OPPORTUNITY, delegate: self)
                .requestRealm(requestModel: formalityEntity)
        } else {
                AlertDialog.show(title: "Error", body: "Ingrese toda la información de contacto y pago", view: mViewController)
        }
    }
    
    public func successAddOpportunity(requestUrl : String, opportunityResponse : OpportunityResponse){
        if opportunityResponse.resultObj?.idResult == "0" {
            mDataManager.tx(execute: { (tx) -> Void in
                mFormalityEntity.Prospecto?.IdCuenta = opportunityResponse.CuentaId!
                mFormalityEntity.Prospecto?.IdOportunidad = opportunityResponse.OportunidadId!
                mFormalityEntity.Prospecto?.IdSitio = opportunityResponse.SitioId!
                mFormalityEntity.Prospecto?.IdCotizacion = opportunityResponse.CotizacionId!
                mFormalityEntity.Prospecto?.IdCuentaFactura = opportunityResponse.CuentaFacturaId!
                mFormalityEntity.Prospecto?.CuentaBRM = opportunityResponse.CuentaBRM!
                tx.save(object: mFormalityEntity.Prospecto!)
                tx.save(object: mFormalityEntity)
            })
            mSendOpportunityDelegate.onSuccessSendOpportunity()
        } else {
            super.onErrorLoadResponse(requestUrl: requestUrl, messageError: (opportunityResponse.resultObj?.descriptionValue)!)
        }
    }
    
    override public func onSuccessLoadResponse(requestUrl: String, response: BaseResponse) {
        AlertDialog.hideOverlay()
        if requestUrl == ApiDefinition.WS_SEND_OPPORTUNITY {
            successAddOpportunity(requestUrl: requestUrl, opportunityResponse: response as! OpportunityResponse)
        }
    }
    
    public func generateContacto(contactoBean : ContactoBean) -> ContactoBean{
        let contactoCryptBean : ContactoBean = ContactoBean()
        contactoCryptBean.Nombre = Security.crypt(text: contactoBean.Nombre)
        contactoCryptBean.ApellidoPaterno = Security.crypt(text: contactoBean.ApellidoPaterno)
        contactoCryptBean.ApellidoMaterno = Security.crypt(text: contactoBean.ApellidoMaterno)
        contactoCryptBean.Celular = Security.crypt(text: contactoBean.Celular)
        contactoCryptBean.MedioContacto = Security.crypt(text: contactoBean.MedioContacto)
        contactoCryptBean.CorreoElectronico = Security.crypt(text: contactoBean.CorreoElectronico)
        contactoCryptBean.OtroTelefono = Security.crypt(text: contactoBean.OtroTelefono)
        contactoCryptBean.Telefono = Security.crypt(text: contactoBean.Telefono)
        contactoCryptBean.RFC = Security.crypt(text: contactoBean.RFC)
        contactoCryptBean.TipoPersona = contactoBean.TipoPersona
        contactoCryptBean.FechaNacimiento = contactoBean.FechaNacimiento
        return contactoCryptBean
    }
    
    public func generateMetodoPago(metodoPagoBean : MetodoPagoBean) -> MetodoPagoBean {
        let metodoPagoCrypBean : MetodoPagoBean = MetodoPagoBean()
        metodoPagoCrypBean.NombreTitular = Security.crypt(text: metodoPagoBean.NombreTitular)
        metodoPagoCrypBean.ApellidoPaternoTitular = Security.crypt(text: metodoPagoBean.ApellidoPaternoTitular)
        metodoPagoCrypBean.ApellidoMaternoTitular = Security.crypt(text: metodoPagoBean.ApellidoPaternoTitular)
        metodoPagoCrypBean.Metodo = Security.crypt(text: metodoPagoBean.Metodo)
        metodoPagoCrypBean.NumeroTarjeta = Security.crypt(text: metodoPagoBean.NumeroTarjeta)
        metodoPagoCrypBean.TipoTarjeta = Security.crypt(text: metodoPagoBean.TipoTarjeta)
        metodoPagoCrypBean.VencimientoAnio = Security.crypt(text: metodoPagoBean.VencimientoAnio)
        metodoPagoCrypBean.VencimientoMes = Security.crypt(text: metodoPagoBean.VencimientoMes)
        return metodoPagoCrypBean
    }
    
    
}
