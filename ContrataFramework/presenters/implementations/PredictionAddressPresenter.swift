//
//  PredictionAddressPresenter.swift
//  EstrategiaDigital
//
//  Created by Charls Salazar on 27/06/17.
//  Copyright © 2017 Charls Salazar. All rights reserved.
//

import UIKit
import Alamofire
import ObjectMapper
import AlamofireObjectMapper
import BaseClases

public class PredictionAddressPresenter : BaseEstrategiaPresenter {
    
    public var mPredictionDelegate : PredictionAddressDelegate?
    public var request : Alamofire.Request?
    
    private let baseURLString = "https://maps.googleapis.com/maps/api/place/autocomplete/json"
    private let baseDetailtsURLString = "https://maps.googleapis.com/maps/api/place/details/json"
    
    public init(delegate : PredictionAddressDelegate) {
        super.init(viewController: BaseViewController())
        self.mPredictionDelegate = delegate
    }
    
    public func getPredictionAddress(keyword: NSString){
        if ConnectionUtils.isConnectedToNetwork(){
            self.mPredictionDelegate?.onSendingPrediction()
            
            let escapedString = keyword.addingPercentEncoding(withAllowedCharacters: .urlHostAllowed)
            let urlString = "\(baseURLString)?key=\(Strings.GOOGLE_MAP_KEY)&language=es&input=\(String(describing: escapedString!))"
            
            request = Alamofire.request(urlString, method: HTTPMethod.get, parameters: nil, encoding: JSONEncoding.default,headers: nil).responseObject {
                
                (response: DataResponse<DirectionResponse>) in
                
                switch response.result {
                    
                case .success:
                    
                    let objectReponse : DirectionResponse = response.result.value!
                    if(!objectReponse.predictionData.isEmpty){
                        self.mPredictionDelegate?.onSuccessPrediction(collectionAddress: objectReponse.predictionData)
                    }else{
                        self.mPredictionDelegate?.onErrorPrediction(errorMessage: "Ocurrio un error al consultar google maps")
                    }
                case .failure(_):
                    self.mPredictionDelegate?.onErrorPrediction(errorMessage: "Ocurrio un error al consultar la BD")
                }
            }
            
        } else {
            self.mPredictionDelegate?.onErrorConnection()
        }
    }
    
    public func getLatLon(placeId : String){
        if ConnectionUtils.isConnectedToNetwork(){
            self.mPredictionDelegate?.onSendingGeometry()
            
            let urlString = "\(baseDetailtsURLString)?placeid=\(placeId)&key=\(Strings.GOOGLE_MAP_KEY)"
            
            request = Alamofire.request(urlString, method: HTTPMethod.get, parameters: nil, encoding: JSONEncoding.default,headers: nil).responseObject {
                
                (response: DataResponse<GeometryResponse>) in
                
                switch response.result {
                    
                case .success:
                    
                    let objectReponse : GeometryResponse = response.result.value!
                    if((objectReponse.result?.geometry?.location?.lat != nil)){
                        self.mPredictionDelegate?.onSuccessGeometry(geoLatLon: (objectReponse.result?.geometry!)!, formatAddress: (objectReponse.result?.formattedAddress)!)
                    }else{
                        self.mPredictionDelegate?.onErrorGeometry(errorMessage: "Ocurrio un error al consultar google maps")
                    }
                case .failure(_):
                    self.mPredictionDelegate?.onErrorGeometry(errorMessage: "Ocurrio un error al consultar la BD")
                }
            }
            
        } else {
            self.mPredictionDelegate?.onErrorConnectionGeometry()
        }
    }
    
}
