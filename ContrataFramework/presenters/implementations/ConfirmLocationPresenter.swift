//
//  ConfirmLocationPresenter.swift
//  EstrategiaDigital
//
//  Created by Jorge Hdez VIlla on 25/10/17.
//  Copyright © 2017 Charls Salazar. All rights reserved.
//

import UIKit
import BaseClases

public protocol ConfirmLocationDelegate: NSObjectProtocol {
    func onSuccessLoadDireccionBean(direccionBean : DireccionBean)
    func onSuccessSaveDireccionBean()
}

public class ConfirmLocationPresenter: BaseEstrategiaPresenter {
    
    public var mConfirmLocationDelegate : ConfirmLocationDelegate!
    public var mDireccionBean : DireccionBean!
    
    public init(viewController: BaseViewController, confirmLocationDelegate : ConfirmLocationDelegate) {
        super.init(viewController: viewController)
        mConfirmLocationDelegate = confirmLocationDelegate
    }
    
    public func loadDireccionBean(){
        if mViewController.hasExtra(key: KeysEnum.EXTRA_DIRECCION_BEAN) {
            mDireccionBean = mViewController.extras[KeysEnum.EXTRA_DIRECCION_BEAN] as! DireccionBean
            mConfirmLocationDelegate.onSuccessLoadDireccionBean(direccionBean: mDireccionBean)
        }
    }
    
    public func confirmDirrecionBean(direccionBean : DireccionBean){
        mFormalityEntity = FormalityEntity()
        mDireccionBean.zipCode = direccionBean.zipCode
        mDireccionBean.street = direccionBean.street
        mDireccionBean.state = direccionBean.state
        mDireccionBean.city = direccionBean.city
        mDireccionBean.colony = direccionBean.colony
        mDireccionBean.district = direccionBean.district
        mDireccionBean.noInt = direccionBean.noInt
        mDireccionBean.noExt = direccionBean.noExt
        mDataManager.tx(execute: { (tx) -> Void in
            mDataManager.deleteFormalityEntity()
            mFormalityEntity.Prospecto = ProspectoBean()
            mFormalityEntity.Prospecto?.Propuesta = PropuestaBean()
            mFormalityEntity.Prospecto?.Vendedor = generateVendedorBean()
            mFormalityEntity.Prospecto?.Firma = FirmaBean()
            mFormalityEntity.Prospecto?.Direccion = mDireccionBean
            mFormalityEntity.Prospecto?.Firma?.Referencias?.Referido.append(generateReferenciaBean())
            tx.save(object: (mFormalityEntity.Prospecto?.Firma?.Referencias)!)
            tx.save(object: mFormalityEntity)
        })
        mDireccionBean = DireccionBean(value: mDireccionBean)
        mConfirmLocationDelegate.onSuccessSaveDireccionBean()
    }
    
    public func generateReferenciaBean() -> ReferidoBean {
        let referidoBean : ReferidoBean = ReferidoBean()
        referidoBean.Nombre = UUID().uuidString
        referidoBean.ApellidoPaterno = UUID().uuidString
        referidoBean.ApellidoMaterno = UUID().uuidString
        referidoBean.Telefono = "5555555555";
        referidoBean.Parentesco = "Otro";
        return referidoBean
    }
    
    public func generateVendedorBean() -> VendedorBean {
        let vendedorBean : VendedorBean = VendedorBean()
        vendedorBean.Canal = "Ecommerce";
        vendedorBean.IdEmpleado = "a0I610000056SBk";
        vendedorBean.SubCanal = "AppMovil";
        vendedorBean.FolioContrato = "";
        vendedorBean.IdDispositivo = "" ///TOKEN NOTIFICATIONS
        vendedorBean.IMEI = DeviceUtils.devideID()
        vendedorBean.IdDistrital = "";
        vendedorBean.IdEmpleadoDistrital = "";
        vendedorBean.OrigenApp = "true";
        vendedorBean.ventaExpress = "false";
        vendedorBean.aprobarVentaExpress = "NA";
        vendedorBean.EsComisionable = "false";
        vendedorBean.UsuarioLogueado = "000002";
        vendedorBean.EmpleadoDistribuidor = "NO COMISIONABLE";
        vendedorBean.ModeloCelular = "IPhone"
        return vendedorBean
    }

}
