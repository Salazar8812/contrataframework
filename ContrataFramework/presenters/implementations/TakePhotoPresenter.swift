//
//  TakePhotoPresenter.swift
//  EstrategiaDigital
//
//  Created by Jorge Hdez VIlla on 05/11/17.
//  Copyright © 2017 Charls Salazar. All rights reserved.
//

import UIKit
import BaseClases
public class TakePhotoPresenter: BaseEstrategiaPresenter {

    public func saveImages(images : [UIImage?]){
        var count = 0
        mDataManager.tx(execute: { (tx) -> Void in
            mFormalityEntity.imagenes.removeAll()
        })
        for image in images {
            if image != nil {
                if let base64String = UIImageJPEGRepresentation(imageWithSize(image: image!, size: CGSize(width: 1200, height: 800)), 0.85)?.base64EncodedString() {
                    mDataManager.tx(execute: { (tx) -> Void in
                        let imagen : Imagen = Imagen()
                        if count == 0 {
                            imagen.name = "Identificacion1.jpeg"
                        } else if count == 1 {
                            imagen.name = "Identificacion2.jpeg"
                        } else {
                            imagen.name = "ComprobanteDomicilio.jpeg"
                        }
                        imagen.type = "Identificación del representante legal"
                        imagen.image = base64String
                        if let thumbnailBase64String = UIImageJPEGRepresentation(imageWithSize(image: image!, size: CGSize(width: 1200, height: 800)), 0.65)?.base64EncodedString() {
                            imagen.thumbnail = thumbnailBase64String
                        }
                        imagen.syncStatus = SyncStatus.NOT_SYNCHRONIZED
                        mFormalityEntity.imagenes.append(imagen)
                        tx.save(object: imagen)
                        tx.save(object: mFormalityEntity)
                    })
                }
            }
            count = count + 1
        }
    }
    
    public func imageWithSize(image: UIImage, size:CGSize) -> UIImage {
        var scaledImageRect = CGRect.zero;
        
        let aspectWidth:CGFloat = size.width / image.size.width;
        let aspectHeight:CGFloat = size.height / image.size.height;
        let aspectRatio:CGFloat = min(aspectWidth, aspectHeight);
        
        scaledImageRect.size.width = image.size.width * aspectRatio;
        scaledImageRect.size.height = image.size.height * aspectRatio;
        scaledImageRect.origin.x = (size.width - scaledImageRect.size.width) / 2.0;
        scaledImageRect.origin.y = (size.height - scaledImageRect.size.height) / 2.0;
        
        UIGraphicsBeginImageContextWithOptions(size, false, 0);
        
        image.draw(in: scaledImageRect);
        
        let scaledImage = UIGraphicsGetImageFromCurrentImageContext();
        UIGraphicsEndImageContext();
        
        return scaledImage!;
    }
}
