//
//  AddonSelectorPresenter.swift
//  EstrategiaDigital
//
//  Created by Charls Salazar on 25/10/17.
//  Copyright © 2017 Charls Salazar. All rights reserved.
//

import UIKit
import BaseClases

public protocol AddonsSelectorDelegate: NSObjectProtocol{
    func onSuccessLoadPlan(plan: Plans)
    func onSuccessAddons(adicionalesBean: [AdicionalesBean])
    func onSuccessSaveProposalBean()
    func onCalculateTotalAmount(priceList : Double, priceSoonPayment : Double)
}

public class AddonSelectorPresenter: BaseEstrategiaPresenter {
    
    public var mPlanSelected : Plans!
    public var mAddonSelectorDelegate : AddonsSelectorDelegate!
    public var mArrIncludeServices : [ArrServiciosIncluidos] = []
    public var planSoonPrice : Double = 0
    
    public init(viewController: BaseViewController, mAddonSelectorDelegate : AddonsSelectorDelegate){
        super.init(viewController: viewController)
        self.mAddonSelectorDelegate = mAddonSelectorDelegate
    }
    
    public func loadPlan(){
        if mViewController.hasExtra(key: KeysEnum.EXTRA_PLAN) {
            mPlanSelected = (mViewController.extras[KeysEnum.EXTRA_PLAN] as! Plans)
            mAddonSelectorDelegate.onSuccessLoadPlan(plan: mPlanSelected!)
        }
    }
    
    public func generateAmountPrice(adicionalsSelected : [AdicionalesBean]){
        var cotServicioProductoList : [CotServicioProducto] = []
        for serviceIncluyed in mArrIncludeServices {
            var productos : [AdicionalesBean] = []
            productos.append(contentsOf: Array(serviceIncluyed.arrProductosIncluidos) as [AdicionalesBean])
            productos.append(contentsOf: adicionalsSelected)
        
            for producto in productos {
                if producto.getID() != "" {
                    if producto.getAdicionalType() == AddicionalBeanType.PRODUCT_INCLUYED {
                        let cotServicioProducto : CotServicioProducto = CotServicioProducto(producto : producto)
                        cotServicioProducto.tipoProducto = AddicionalBeanType.PRODUCT_INCLUYED
                        cotServicioProductoList.append(cotServicioProducto)
                    }
                }
            }
        }
        
        for servicioAdditional in adicionalsSelected {
            if servicioAdditional.getAdicionalType() == AddicionalBeanType.SERVICE {
                let arrServicioAdicionalBean : ArrServiciosAdicionales = servicioAdditional as! ArrServiciosAdicionales
                for productoIncluido in arrServicioAdicionalBean.arrProductosIncluidos {
                    if productoIncluido.getID() != "" {
                        let cotServicioProducto : CotServicioProducto = CotServicioProducto(producto: productoIncluido)
                        cotServicioProducto.tipoProducto = AddicionalBeanType.PRODUCT_ADDITIONAL
                        cotServicioProductoList.append(cotServicioProducto)
                    }
                }
            } else if servicioAdditional.getAdicionalType() == AddicionalBeanType.PRODUCT_ADDITIONAL {
                if servicioAdditional.getAdicionalName().count > (mPlanSelected.planName?.count)! {
                    let name = servicioAdditional.getAdicionalName().substring(to: (mPlanSelected.planName?.endIndex)!)
                    if mPlanSelected.planName == name {
                        let cotServicioProducto : CotServicioProducto = CotServicioProducto(producto : servicioAdditional)
                        cotServicioProductoList.append(cotServicioProducto)
                    }
                }
            }
        }
        
        var acumulador : Double = 0
        for servicioProducto in cotServicioProductoList {
            if (servicioProducto.tipoProducto == AddicionalBeanType.PRODUCT_INCLUYED || servicioProducto.tipoProducto == AddicionalBeanType.PRODUCT_ADDITIONAL) {
                if servicioProducto.precioUnitarioBase > 0 {
                    if servicioProducto.impuesto2 > 0 {
                        acumulador = acumulador + (1.03 * servicioProducto.precioUnitarioProntoPago)
                    } else {
                        acumulador = acumulador + servicioProducto.precioUnitarioProntoPago
                    }
                }
            }
        }
        acumulador = acumulador * 1.16;
        
        var priceList : Double = 0
        if planSoonPrice == 0 {
            planSoonPrice = acumulador
            priceList = Double(mPlanSelected.amountL!)!
        } else {
            priceList = Double(mPlanSelected.amountL!)! + ( acumulador - planSoonPrice )
        }
        
        mDataManager.tx(execute: { (tx) -> Void in
            mFormalityEntity?.totalPriceSoonPayment = acumulador
            mFormalityEntity?.totalPriceList = priceList
            tx.save(object: mFormalityEntity)
        })
        
        mAddonSelectorDelegate.onCalculateTotalAmount(priceList: priceList, priceSoonPayment : acumulador)
    }
    
    override func onRequestWs() {
        AlertDialog.showOverlay()
    }
    
    public func loadAddons(){
        let requestModel : PlainDetailRequest = PlainDetailRequest(idPlan: (mPlanSelected?.planId)!)
        RetrofitManager<PlanDetailResponse>.init(requestUrl: ApiDefinition.WS_PLAIN_DETAIL, delegate: self).request(requestModel: requestModel)
    }
    
    public func onSuccessAddons(planDetailResponse : PlanDetailResponse){
        var adicionalesBean : [AdicionalesBean] = []
        
        for serviceAdicional in planDetailResponse.arrServiciosAdicionales{
            if serviceAdicional.image != "" {
                adicionalesBean.append(serviceAdicional)
            }
        }
        for productoAdicional in planDetailResponse.arrProductosAdicionales{
            if productoAdicional.image != "" {
                adicionalesBean.append(productoAdicional)
            }
        }
        mArrIncludeServices = planDetailResponse.arrServiciosIncluidos
        mAddonSelectorDelegate?.onSuccessAddons(adicionalesBean: adicionalesBean)
    }
    
    public func onSeelectAdicionals(mAdicionalsSelected : [AdicionalesBean]){
        var addonServicesBean : [ArrServiciosAdicionales] = []
        var arrAddonProd : [ArrProductosAdicionales] = []
        
        for adicional in mAdicionalsSelected {
            if adicional.getAdicionalType() == AddicionalBeanType.SERVICE {
                addonServicesBean.append(adicional as! ArrServiciosAdicionales)
            } else {
                arrAddonProd.append(adicional as! ArrProductosAdicionales)
            }
        }
        generateProposal(arrIncludeServices: mArrIncludeServices, addonServicesBean: addonServicesBean, arrAddonProd: arrAddonProd)
    }
    
    override public func onSuccessLoadResponse(requestUrl: String, response: BaseResponse) {
        if (requestUrl == ApiDefinition.WS_PLAIN_DETAIL){
            onSuccessAddons(planDetailResponse: response as! PlanDetailResponse)
        }
        AlertDialog.hideOverlay()
    }
    
    override public func onErrorLoadResponse(requestUrl: String, messageError: String) {
        AlertDialog.show( title: "Error", body: StringDialogs.dialog_error_intern, view: mViewController)
        AlertDialog.hideOverlay()
    }
    
    public func generateProposal(arrIncludeServices : [ArrServiciosIncluidos], addonServicesBean : [ArrServiciosAdicionales], arrAddonProd : [ArrProductosAdicionales]){
        
        let proposeBean : PropuestaBean = PropuestaBean()
        proposeBean.TipoPago = "Prepago"
        //            proposeBean.Oferta = (saleRelease.typePlain?.tipo!)! ?? Porque comentada?
        let cotSitioPlanBean : CotSitioPlanBean = CotSitioPlanBean()
        cotSitioPlanBean.NombrePlan = mPlanSelected.planName!
        cotSitioPlanBean.DP_Plan = mPlanSelected.planId!
        proposeBean.Cot_SitioPlan = cotSitioPlanBean
        
        //// SERVICES INCLUYED
        var indexServices = 0
        for item in arrIncludeServices {
            let cotPlanServicioBean : CotPlanServicioBean = CotPlanServicioBean()
            cotPlanServicioBean.DP_PlanServicio = item.id
            cotPlanServicioBean.EsServicioAdicional = "false"
            cotPlanServicioBean.NombreServicio = item.nombre
            cotPlanServicioBean.Tipo = "Incluido"
            cotPlanServicioBean.DP_PromocionPlan = ""
            ///// PRODUCTS INCLUYED
            var arrCotServicioProductoBean : [CotServicioProductoBean] = []
            for subitem in item.arrProductosIncluidos {
                if subitem.id != "" {
                    let cotServicioProductoBean : CotServicioProductoBean = CotServicioProductoBean(arrProductoIncluido: subitem)
                    arrCotServicioProductoBean.append(cotServicioProductoBean)
                }
            }
            ///// PRODUCTS ADITIONALS
            for subitem in arrAddonProd {
                if subitem.id != "" && subitem.cantidad != "" {
                    if subitem.nombre.count > (mPlanSelected.planName?.count)! {
                        if indexServices <= 2 {
                            let planName = cotPlanServicioBean.NombreServicio.replacingOccurrences(of: "4K", with: "").trim()
                            let productName = subitem.nombre.substring(to: (planName.endIndex))
                            if planName == productName {
                                let cotServicioProductoBean : CotServicioProductoBean = CotServicioProductoBean(arrProductoAdicional: subitem)
                                arrCotServicioProductoBean.append(cotServicioProductoBean)
                            }
                        }
                    }
                }
            }
            indexServices = indexServices + 1
            
            cotPlanServicioBean.Cot_ServicioProducto.append(objectsIn: arrCotServicioProductoBean)
            proposeBean.Cot_PlanServicio.append(cotPlanServicioBean)
        }
        
        //// SERVICES ADITIONALS
        for item in addonServicesBean {
            let cotPlanServicioBean : CotPlanServicioBean = CotPlanServicioBean()
            cotPlanServicioBean.DP_PlanServicio = item.id
            cotPlanServicioBean.EsServicioAdicional = "true"
            cotPlanServicioBean.NombreServicio = item.nombre
            cotPlanServicioBean.Tipo = "Adicional"
            cotPlanServicioBean.DP_PromocionPlan = ""
            var arrCotServicioProductoBean : [CotServicioProductoBean] = []
            
            ////// PRODUCTS INCLUYED
            for subitem in item.arrProductosIncluidos {
                if subitem.id != "" {
                    let cotServicioProductoBean : CotServicioProductoBean = CotServicioProductoBean(arrProductoIncluido : subitem)
                    arrCotServicioProductoBean.append(cotServicioProductoBean)
                }
            }
            
            cotPlanServicioBean.Cot_ServicioProducto.append(objectsIn: arrCotServicioProductoBean)
            proposeBean.Cot_PlanServicio.append(cotPlanServicioBean)
        }
        
        var addonsName : [NameData] = []
        for item in addonServicesBean {
            addonsName.append(NameData(name: item.nombre))
        }
        for item in arrAddonProd {
            addonsName.append(NameData(name: item.nombre))
        }
        
        mDataManager.tx(execute: { (tx) -> Void in
            mFormalityEntity.Prospecto?.Propuesta?.TipoPago = proposeBean.TipoPago
            mFormalityEntity.Prospecto?.Propuesta?.Cot_PlanServicio.removeAll()
            mFormalityEntity.addonsServices.removeAll()
            mFormalityEntity.addonsServices.append(contentsOf: addonsName)
            mFormalityEntity.Prospecto?.Propuesta?.Cot_PlanServicio.append(contentsOf: proposeBean.Cot_PlanServicio)
            mFormalityEntity.Prospecto?.Propuesta?.Cot_SitioPlan = proposeBean.Cot_SitioPlan
            
            tx.save(object: (mFormalityEntity.Prospecto?.Propuesta)!)
            tx.save(object: mFormalityEntity.Prospecto!)
            tx.save(object: mFormalityEntity)
        })
        mAddonSelectorDelegate?.onSuccessSaveProposalBean()
    }
    
}
