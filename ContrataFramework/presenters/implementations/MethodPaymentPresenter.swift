//
//  MethodPaymentPresenter.swift
//  EstrategiaDigital
//
//  Created by Jorge Hdez VIlla on 30/10/17.
//  Copyright © 2017 Charls Salazar. All rights reserved.
//

import UIKit
import BaseClases
public class MethodPaymentPresenter: BaseEstrategiaPresenter {
    
    public func saveMethodPayment(metodoPagoBean : MetodoPagoBean){
        mDataManager.tx(execute: { (tx) -> Void in
            mFormalityEntity.Prospecto?.Firma?.MetodoPago = metodoPagoBean
            tx.save(object: mFormalityEntity)
        })
    }
    
}
