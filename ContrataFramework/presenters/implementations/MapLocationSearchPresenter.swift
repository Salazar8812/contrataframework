//
//  MapLocationSearchPresenter.swift
//  EstrategiaDigital
//
//  Created by Jorge Hdez VIlla on 19/09/17.
//  Copyright © 2017 Charls Salazar. All rights reserved.
//

import UIKit
import CoreLocation
import BaseClases
public protocol MapLocationSearchDelegate : NSObjectProtocol {
    func onErrorCoverageValidate()
    func onSuccessLoadFactibility(direccionBean : DireccionBean)
}

public class MapLocationSearchPresenter: BaseEstrategiaPresenter, CLLocationManagerDelegate{
    
    public var mMapLocationSearchDelegate : MapLocationSearchDelegate!
    public var mPosition : CLLocationCoordinate2D!
    
    public init(viewController: BaseViewController, mapLocationSearchDelegate : MapLocationSearchDelegate) {
        super.init(viewController: viewController)
        self.mMapLocationSearchDelegate = mapLocationSearchDelegate
    }
    
    override public func viewDidLoad() {
        requestLocation()
    }
    
    public func requestLocation(){
        let locationManager = CLLocationManager()
        locationManager.requestAlwaysAuthorization()
        locationManager.requestWhenInUseAuthorization()
        
        if CLLocationManager.locationServicesEnabled() {
            locationManager.delegate = self
            locationManager.desiredAccuracy = kCLLocationAccuracyBest
            locationManager.requestAlwaysAuthorization()
            locationManager.startUpdatingLocation()
        }
    }
    
    public func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        let locValue:CLLocationCoordinate2D = manager.location!.coordinate
        print("locations = \(locValue.latitude) \(locValue.longitude)")
    }
    
    public func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        print(error)
    }
    
    public func locationManager(manager: CLLocationManager!, didUpdateLocations locations: [AnyObject]!) {
    }

    public func validateCoverage(position : CLLocationCoordinate2D!){
        mPosition = position
        let requestModel : ValidateCoverageRequest = ValidateCoverageRequest(latitude: "\(position.latitude)", longitude: "\(position.longitude)")
        RetrofitManager<ValidateCoverageResponse>.init(requestUrl: ApiDefinition.WS_COVERAGE_VALIDATE, delegate: self).request(requestModel: requestModel)
    }
    
    public func successLoadCoverageValidate(requestUrl : String, validateCoverageResponse : ValidateCoverageResponse){
        if validateCoverageResponse.feasibility != nil && validateCoverageResponse.feasibility.feasible != "false" {
            let direccionBean : DireccionBean = DireccionBean()
            let feasibility : Feasibility = validateCoverageResponse.feasibility!
            direccionBean.cluster = feasibility.cluster
            direccionBean.district = feasibility.district
            direccionBean.authorizationWhitoutCoverage = "false"
            direccionBean.place = feasibility.city
            direccionBean.idRegion = feasibility.regionId
            direccionBean.region = feasibility.region
            direccionBean.feasibility = "0"
            direccionBean.feasible = feasibility.feasible
            direccionBean.zoneValue = feasibility.zoneValue
            direccionBean.latitude = "\(mPosition.latitude)"
            direccionBean.longitude = "\(mPosition.longitude)"
            mMapLocationSearchDelegate.onSuccessLoadFactibility(direccionBean: direccionBean)
        } else{
            mMapLocationSearchDelegate.onErrorCoverageValidate()
        }
    }
    
    override public func onSuccessLoadResponse(requestUrl : String, response : BaseResponse){
        AlertDialog.hideOverlay()
        if (requestUrl == ApiDefinition.WS_COVERAGE_VALIDATE){
            successLoadCoverageValidate(requestUrl: requestUrl, validateCoverageResponse: response as! ValidateCoverageResponse)
        }
    }
    
    override public func onErrorLoadResponse(requestUrl : String, messageError : String){
        AlertDialog.hideOverlay()
        if (requestUrl == ApiDefinition.WS_COVERAGE_VALIDATE && messageError != ""){
            mMapLocationSearchDelegate.onErrorCoverageValidate()
        } else {
            AlertDialog.show( title: "Error", body: StringDialogs.dialog_error_intern, view: mViewController)
        }
    }

    
}
