//
//  PaymentMethodViewController.swift
//  EstrategiaDigital
//
//  Created by Charls Salazar on 21/07/17.
//  Copyright © 2017 Charls Salazar. All rights reserved.
//

import UIKit

public protocol PaymentMethodDelegate : NSObjectProtocol {
    func onSuccessLoadInfoPayment(metodoPagoBean : MetodoPagoBean)
}

class PaymentMethodViewController: BaseViewController, PaymentMethodDelegate {

    @IBOutlet weak var mTypeMethodSegmentControl: UISegmentedControl!
    @IBOutlet weak var mContentView: UIView!
    
    var mPaymentMethodPresenter : MethodPaymentPresenter!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        loadMethodCashPay()
        
        self.mTypeMethodSegmentControl.setTitleTextAttributes([NSForegroundColorAttributeName: UIColor.white], for: .normal)
        
        self.mTypeMethodSegmentControl.setTitleTextAttributes([NSForegroundColorAttributeName: UIColor.white], for: UIControlState.selected)
    }
    
    override func getPresenter() -> BasePresenter? {
        mPaymentMethodPresenter = MethodPaymentPresenter(viewController: self)
        return mPaymentMethodPresenter
    }
    
    func loadMethodCashPay(){
        let cashMethodPayView = self.storyboard?.instantiateViewController(withIdentifier: "CashMethodPayViewController") as! CashMethodPayViewController
        cashMethodPayView.view.frame = mContentView.bounds
        cashMethodPayView.mMethodPaymentDelegate = self
        mContentView.addSubview(cashMethodPayView.view)
        addChildViewController(cashMethodPayView)
        cashMethodPayView.didMove(toParentViewController: self)
    }
    
    func loadMethodCardPay(){
        let cashMethodPayView = self.storyboard?.instantiateViewController(withIdentifier: "CardMethodPayViewController") as! CardMethodPayViewController
        cashMethodPayView.view.frame = mContentView.bounds
        cashMethodPayView.mMethodPaymentDelegate = self
        mContentView.addSubview(cashMethodPayView.view)
        addChildViewController(cashMethodPayView)
        cashMethodPayView.didMove(toParentViewController: self)
    }
    
    func onSuccessLoadInfoPayment(metodoPagoBean: MetodoPagoBean) {
        mPaymentMethodPresenter.saveMethodPayment(metodoPagoBean: metodoPagoBean)
        resultValue = .RESULT_OK
        ViewControllerUtils.popViewController(viewController: self)
    }

    @IBAction func mTypeMethodSegmentControl(_ sender: Any) {
        switch mTypeMethodSegmentControl.selectedSegmentIndex {
        case 0:
            loadMethodCashPay()
            break
        case 1:
            loadMethodCardPay()
            break
        default:
            break
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
}
