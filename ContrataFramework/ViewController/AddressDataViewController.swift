//
//  AddressDataViewController.swift
//  EstrategiaDigital
//
//  Created by Jorge Hdez Villa on 09/08/17.
//  Copyright © 2017 Charls Salazar. All rights reserved.
//

import UIKit

class AddressDataViewController: BaseEstrategiaViewController, CheckGroupDelegate, ControllerResultDelegate {
    
    @IBOutlet var mCheckButtons: [CheckBoxButton]!
    @IBOutlet weak var mEqualAddressCheckboxButton: CheckBoxButton!
    @IBOutlet weak var mOtherAddressCheckBoxButton: CheckBoxButton!
    
    @IBOutlet weak var mAddressFormView: UIView!
    @IBOutlet weak var mZipCodeTextField: IconTextField!
    @IBOutlet weak var mStreetTextField: IconTextField!
    @IBOutlet weak var mNoExtTextField: IconTextField!
    @IBOutlet weak var mNoIntTextField: IconTextField!
    @IBOutlet weak var mColonyTextField: IconTextField!
    @IBOutlet weak var mCityTextField: IconTextField!
    @IBOutlet weak var mDelegationTextField: IconTextField!
    @IBOutlet weak var mStateTextField: IconTextField!
    
    var mIsFirstLoad : Bool! = true

    var mAddressCheckBoxGroup : CheckBoxGroup!
    var mFormValidator : FormValidator!
    var mAddressDataPresenter : AddressDataPresenter!
    
    override func viewDidAppear(_ animated: Bool) {
        if mIsFirstLoad {
            mIsFirstLoad = false
            ViewUtils.setVisibility(view: mAddressFormView, visibility: .GONE)
            ViewUtils.setVisibility(view: mAddressFormView, visibility: .VISIBLE)
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        mAddressCheckBoxGroup = CheckBoxGroup(checksBox: mCheckButtons, delegate: self)
    
        ViewUtils.setVisibility(view: mAddressFormView, visibility: .INVISIBLE)
        mAddressCheckBoxGroup.checkBoxSelected = mEqualAddressCheckboxButton
        
        mFormValidator = FormValidator(showAllErrors: true)
        mFormValidator.addValidators(validators:
            TextFieldValidator(textField: mZipCodeTextField, regex: RegexEnum.NOT_EMPTY, minCharacters : 5, maxCharacters : 5, messageError : ""),
            TextFieldValidator(textField: mStreetTextField, regex: RegexEnum.NOT_EMPTY),
            TextFieldValidator(textField: mNoExtTextField, regex: RegexEnum.NOT_EMPTY),
            TextFieldValidator(textField: mColonyTextField, regex: RegexEnum.NOT_EMPTY),
            TextFieldValidator(textField: mCityTextField, regex: RegexEnum.NOT_EMPTY),
            TextFieldValidator(textField: mDelegationTextField, regex: RegexEnum.NOT_EMPTY),
            TextFieldValidator(textField: mStateTextField, regex: RegexEnum.NOT_EMPTY)
        )
        
        loadAddressInfo()
    }
    
    override func getPresenter() -> BasePresenter? {
        mAddressDataPresenter = AddressDataPresenter(viewController: self)
        return mAddressDataPresenter
    }
    
    func loadAddressInfo(){
        let direccionFacturacion : DireccionFacturacionBean? = mFormalityEntity.Prospecto?.Firma?.DireccionFacturacion
        if direccionFacturacion != nil {
            if direccionFacturacion?.MismaDireccionInstalacion == "false" {
                mZipCodeTextField.text = direccionFacturacion?.CodigoPostal
                mStreetTextField.text = direccionFacturacion?.Calle.uppercased()
                mNoExtTextField.text = direccionFacturacion?.NumeroExterior.uppercased()
                mNoIntTextField.text = direccionFacturacion?.NumeroInterior
                mColonyTextField.text = direccionFacturacion?.Colonia.uppercased()
                mCityTextField.text = direccionFacturacion?.Ciudad.uppercased()
                mDelegationTextField.text = direccionFacturacion?.DelegacionMunicipio.uppercased()
                mStateTextField.text = direccionFacturacion?.Estado.uppercased()
                mOtherAddressCheckBoxButton.isChecked = true
                mEqualAddressCheckboxButton.isChecked = false
                mIsFirstLoad = false
                ViewUtils.setVisibility(view: mAddressFormView, visibility: .VISIBLE)
                mAddressCheckBoxGroup.checkBoxSelected = mOtherAddressCheckBoxButton
//                onCheckBoxButtonClick(checkButton: mOtherAddressCheckBoxButton)
            } else {
                mEqualAddressCheckboxButton.isChecked = true
            }
        } else {
            mEqualAddressCheckboxButton.isChecked = true
        }
    }
    
    @IBAction func onNextButtonClick(_ sender: Any) {
        if mAddressCheckBoxGroup.checkBoxSelected == mEqualAddressCheckboxButton {
            mAddressDataPresenter.saveDireccionFacturacionBean()
            ViewControllerUtils.pushViewControllerWithResult(from: self, to: TakePhotoViewController.self, request: ContractingViewController.REQUEST_CODE_ADD_DOCUMENTS_INFO)
        } else {
            if mFormValidator.isValid() {
                let direccionFacturacion : DireccionFacturacionBean = DireccionFacturacionBean()
                direccionFacturacion.CodigoPostal = (mZipCodeTextField.text?.trim())!
                direccionFacturacion.Calle = (mStreetTextField.text?.trim())!
                direccionFacturacion.NumeroExterior = (mNoExtTextField.text?.trim())!
                direccionFacturacion.NumeroInterior = (mNoIntTextField.text?.trim())!
                direccionFacturacion.Colonia = (mColonyTextField.text?.trim())!
                direccionFacturacion.Ciudad = (mCityTextField.text?.trim())!
                direccionFacturacion.DelegacionMunicipio = (mDelegationTextField.text?.trim())!
                direccionFacturacion.Estado = (mStateTextField.text?.trim())!
                mAddressDataPresenter.saveDirecionFacturacionBean(direccionFacturacionBean: direccionFacturacion)
                ViewControllerUtils.pushViewControllerWithResult(from: self, to: TakePhotoViewController.self, request: ContractingViewController.REQUEST_CODE_ADD_DOCUMENTS_INFO)
            } else {
                AlertDialog.show(title: "Error", body: StringDialogs.dialog_error_empty_fields_new, view: self)
            }
        }
    }
    
    func onCheckBoxButtonClick(checkButton: CheckBoxButton) {
        if checkButton == mEqualAddressCheckboxButton {
            ViewUtils.setVisibility(view: mAddressFormView, visibility: .GONE, controller: self, animated: true)
        } else if checkButton == mOtherAddressCheckBoxButton {
            ViewUtils.setVisibility(view: mAddressFormView, visibility: .NOT_GONE, controller: self, animated: true)
        }
    }
    
    func viewControllerForResult(keyRequest: String, result: ViewControllerResult, data: [String : AnyObject]) {
        if keyRequest == ContractingViewController.REQUEST_CODE_ADD_DOCUMENTS_INFO {
            if result == ViewControllerResult.RESULT_OK {
                resultValue = .RESULT_OK
                resultDelegate?.viewControllerForResult(keyRequest: requestValue, result: resultValue, data: data)
            }
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
}
