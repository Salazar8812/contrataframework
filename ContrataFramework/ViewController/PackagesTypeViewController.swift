//
//  CoverageAcceptViewController.swift
//  EstrategiaDigital
//
//  Created by Charls Salazar on 28/06/17.
//  Copyright © 2017 Charls Salazar. All rights reserved.
//

import UIKit

class PackagesDataSource : BaseMultiCellDataSource<ArrFamily> {
    
    override func getItemTypeCell(item : NSObject) -> BaseTableViewCell? {
        let arrFamily : ArrFamily = item as! ArrFamily
        if arrFamily.nameFamily.lowercased().range(of:"doble") != nil {
            if arrFamily.nameFamily.lowercased().range(of:"tv") != nil {
                return getCell(cell: PackageDobleTVCell.self)
            } else {
                return getCell(cell: PackageDoblePhoneCell.self)
            }
        }
        return getCell(cell: PackageTripleCell.self)
    }
    
}

class PackagesTypeViewController: BaseViewController, UITableViewDelegate, PlansTableViewDelegate, PackageTypeDelegate,
    TableViewCellClickDelegate {
    
    @IBOutlet weak var mHomePackagesContentView: UIView!
    
    //// FIRST BUTTON
    @IBOutlet weak var mHomeButton: UIButton!
    @IBOutlet weak var mHomeTitleLabel: UILabel!
    @IBOutlet weak var mHomeIconImageView: UIImageView!
    
    //// SECOND BUTTON
    @IBOutlet weak var mBusinessButton: UIButton!
    @IBOutlet weak var mTitleBusinessLabel: UILabel!
    @IBOutlet weak var mBusinessIconImageView: UIImageView!
    
    @IBOutlet weak var mPackageListTableView: UITableView!
    var mPackageDataSource : PackagesDataSource!
    var mArrFamily: [ArrFamily]!
    var mArrFamilyBussiness : ArrFamily?
    var mPackagesTypePresenter : PackagesTypePresenter?
    
    @IBOutlet weak var TagSelectYourPackageLabel: UILabel!
    var mTypePlanSelected : String?
    
    @IBOutlet weak var dynamicHeightConstraint: NSLayoutConstraint!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        ViewUtils.setVisibility(view: mHomePackagesContentView, visibility: .GONE)
    
        mBusinessButton.layer.borderColor = UIColor(netHex: Colors.color_grey).cgColor
        mBusinessButton.layer.borderWidth = 1
        mBusinessButton.layer.cornerRadius = 4
        
        mHomeButton.layer.cornerRadius = 4
        mHomeButton.layer.borderWidth = 1
        mHomeButton.layer.borderColor = UIColor(netHex: Colors.color_grey).cgColor
        
        mPackageDataSource = PackagesDataSource(tableView: mPackageListTableView, delegate : self)
        
        mHomeIconImageView.image = mHomeIconImageView.image?.withRenderingMode(.alwaysTemplate)
        mHomeIconImageView.tintColor = UIColor.white
        
        mBusinessIconImageView.image = mBusinessIconImageView.image?.withRenderingMode(.alwaysTemplate)
        mBusinessIconImageView.tintColor = UIColor.white
        
        
    }
    
    override func getPresenter() -> BasePresenter? {
        mPackagesTypePresenter = PackagesTypePresenter(viewController: self, mPackageTypeDelegate: self)
        return mPackagesTypePresenter
    }
    
    @IBAction func mBusinessButton(_ sender: Any) {
        mTypePlanSelected = "Micronegocios"
        ViewUtils.setVisibility(view: TagSelectYourPackageLabel, visibility:.INVISIBLE)
        mHomeButton.backgroundColor = nil
       
        mBusinessButton.backgroundColor  = UIColor(netHex:Colors.color_green_select_plan)
        mBusinessIconImageView.image = mBusinessIconImageView.image?.withRenderingMode(.alwaysTemplate)
        mBusinessIconImageView.tintColor = UIColor(netHex: Colors.color_grey_select_plan)
        mTitleBusinessLabel.textColor = UIColor(netHex: Colors.color_grey)
        
        mHomeIconImageView.image = mHomeIconImageView.image?.withRenderingMode(.alwaysTemplate)
        mHomeIconImageView.tintColor = UIColor.white
        mHomeTitleLabel.textColor = UIColor(netHex: Colors.color_white)
        
        mPackagesTypePresenter?.loadBusinessPackages()
        
    }
    
    @IBAction func mHomeButton(_ sender: Any) {
        mTypePlanSelected = "Residencial"
        ViewUtils.setVisibility(view: TagSelectYourPackageLabel, visibility:.VISIBLE)
        mBusinessButton.backgroundColor  = nil
        
        mHomeButton.backgroundColor = UIColor(netHex: Colors.color_green_select_plan)
        mHomeIconImageView.image = mHomeIconImageView.image?.withRenderingMode(.alwaysTemplate)
        mHomeIconImageView.tintColor = UIColor(netHex: Colors.color_grey_select_plan)
        mHomeTitleLabel.textColor = UIColor(netHex: Colors.color_grey)
        
        mBusinessIconImageView.image = mBusinessIconImageView.image?.withRenderingMode(.alwaysTemplate)
        mBusinessIconImageView.tintColor = UIColor.white
        mTitleBusinessLabel.textColor = UIColor(netHex: Colors.color_white)
        
        mTitleBusinessLabel.textColor = UIColor(netHex: Colors.color_white)
        mPackagesTypePresenter?.loadHomePackages()
    }

    @IBAction func nextButton(_ sender: Any) {
        if mPackageDataSource.itemSelected != nil {
            var extras : [String : AnyObject] = [:]
            extras[KeysEnum.EXTRA_ARR_FAMILY] = mPackageDataSource.itemSelected
            ViewControllerUtils.pushViewController(from: self, to: PackagesViewController.self, extras : extras)
        } else {
            AlertDialog.show(title: "Error", body: "Seleccione un paquete", view: self)
        }
    }
    
    func onItemClick(item: [ArrPlan]) {
        print(item[0].planName)
    }
    
    func onSuccessPMPlanes(arrFamily: [ArrFamily]){
        mArrFamily = arrFamily
        if(mTypePlanSelected == "Micronegocios"){
            mPackageDataSource.update(items: [])
            for item in mArrFamily {
                if(item.nameFamily == Strings.MICRONEGOCIO_TWOPLAY){
                    mPackageDataSource.itemSelected = item
                }
            }
        }else{
            mPackageDataSource.itemSelected = nil
            mPackageDataSource.update(items: mArrFamily)
        }
    }

    func onTableViewCellClick(item: NSObject, cell : UITableViewCell){
        mPackageDataSource.update(items: mArrFamily)
    }
    
}
