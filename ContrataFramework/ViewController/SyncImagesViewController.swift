//
//  SyncImagesViewController.swift
//  EstrategiaDigital
//
//  Created by Jorge Hdez VIlla on 05/11/17.
//  Copyright © 2017 Charls Salazar. All rights reserved.
//

import UIKit

class SyncImagesViewController: BaseEstrategiaViewController, SyncDelegate {
    
    @IBOutlet weak var mImagesTableView: UITableView!

    var mSyncImagesDataSource : BaseDataSource<Imagen, SyncImageCell>!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        mSyncImagesDataSource = BaseDataSource(tableView: mImagesTableView)
        mSyncImagesDataSource.update(items: Array(mFormalityEntity.imagenes))

        let syncTask = SyncTask(delegate: self)
        syncTask.executeTask(formalityId: mFormalityEntity.uuid)
        
    }

    func onSyncStatusChange(){
        mFormalityEntity = mDataManager.queryWhere(object: FormalityEntity.self).findFirst()
        mSyncImagesDataSource.update(items: Array(mFormalityEntity.imagenes))
    }
    
    func onSyncError() {
        mFormalityEntity = mDataManager.queryWhere(object: FormalityEntity.self).findFirst()
        mSyncImagesDataSource.update(items: Array(mFormalityEntity.imagenes))
    }
    
    func onSyncFinish() {
        self.navigationController?.popToRootViewController(animated: true)
        ViewControllerUtils.presentViewController(from: self, to: TrackingTicketViewController.self)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }

    
}
