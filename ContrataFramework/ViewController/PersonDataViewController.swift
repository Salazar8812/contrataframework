//
//  PersonDataViewController.swift
//  EstrategiaDigital
//
//  Created by Jorge Hdez Villa on 07/08/17.
//  Copyright © 2017 Charls Salazar. All rights reserved.
//

import UIKit

class PersonDataViewController: BaseEstrategiaViewController, ControllerResultDelegate {
    
    @IBOutlet weak var mPersonTypeSegmentedControl: UISegmentedControl!
    @IBOutlet weak var mIdentificationTypeTextField: IconTextField!
    @IBOutlet weak var mBusinessNameView: UIView!
    @IBOutlet weak var mIdentificationNumberTextField: IconTextField!
    @IBOutlet weak var mBusinessNameTextField: IconTextField!
    @IBOutlet weak var mNameTextField: IconTextField!
    @IBOutlet weak var mLastNameTextField: IconTextField!
    @IBOutlet weak var mMotherLastNameTextField: IconTextField!
    @IBOutlet weak var mBirthdayTextField: IconTextField!
    @IBOutlet weak var mRfcTextField: IconTextField!
    @IBOutlet weak var mEmailTextField: IconTextField!
    @IBOutlet weak var mCelPhoneTextField: IconTextField!
    @IBOutlet weak var mHomePhoneTextField: IconTextField!
    
    @IBOutlet weak var mTagPhoneHouseLabel: UILabel!
    @IBOutlet weak var mTagBornLabel: UILabel!
    let mIdentificationTypes : [String] = ["IFE", "Licencia", "Pasaporte", "Cédula profesional", "FMM", "INAPAM"]
    let mContactTye : [String] = ["EMAIL", "SMS", "Notificación Push", "Llamada telefónica", "Ninguno"]
    
    var mFormValidator : FormValidator!
    var mPersonDataPresenter : PersonDataPresenter!
    
    @IBOutlet weak var mContactTextField: IconTextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        mContactTextField.inputView = UIView()
        mIdentificationTypeTextField.inputView = UIView()
        mFormValidator = FormValidator(showAllErrors: true)
        mFormValidator.addValidators(validators:
            SegmentedControlValidator(segmentedControl: mPersonTypeSegmentedControl, positionValue : 1, validators:
                TextFieldValidator(textField: mBusinessNameTextField, regex: RegexEnum.NOT_EMPTY)
            ),
            TextFieldValidator(textField: mIdentificationTypeTextField, regex: RegexEnum.NOT_EMPTY),
            TextFieldValidator(textField: mIdentificationNumberTextField, regex: RegexEnum.NOT_EMPTY),
            TextFieldValidator(textField: mNameTextField, regex: RegexEnum.NOT_EMPTY),
            TextFieldValidator(textField: mLastNameTextField, regex: RegexEnum.NOT_EMPTY),
            TextFieldValidator(textField: mMotherLastNameTextField, regex: RegexEnum.NOT_EMPTY),
            TextFieldValidator(textField: mBirthdayTextField, regex: RegexEnum.NOT_EMPTY),
            TextFieldValidator(textField: mEmailTextField, regex: RegexEnum.NOT_EMPTY),
            TextFieldValidator(textField: mEmailTextField, regex: RegexEnum.EMAIL),
            TextFieldValidator(textField: mContactTextField, regex: RegexEnum.NOT_EMPTY),
            TextFieldValidator(textField: mCelPhoneTextField, regex: RegexEnum.NOT_EMPTY),
            TextFieldValidator(textField: mHomePhoneTextField, regex: RegexEnum.NOT_EMPTY, minCharacters: 10, maxCharacters: 10, messageError: ""),
            TextFieldValidator(textField: mRfcTextField, regex: RegexEnum.NOT_EMPTY, messageError: StringDialogs.dialog_error_empty)          //  TextFieldValidator(textField: mRfcTextField, regex: RegexEnum.RFC, messageError: "")
        )
        
        mIdentificationTypeTextField.setTextPicker(elements: mIdentificationTypes)
        
        mContactTextField.setTextPicker(elements: mContactTye)
        mBirthdayTextField.setDatePicker()
        
        ViewUtils.setVisibility(view: mBusinessNameView, visibility: .GONE)
        
        self.mPersonTypeSegmentedControl.setTitleTextAttributes([NSForegroundColorAttributeName: UIColor.white], for: .normal)
        
        self.mPersonTypeSegmentedControl.setTitleTextAttributes([NSForegroundColorAttributeName: UIColor.white], for: UIControlState.selected)
        
        loadContactInfo()
    }

    
    func loadContactInfo(){
        let contactoBean : ContactoBean? = mFormalityEntity.Prospecto?.Contacto
        let infoAdicionalBean : DatosAdicionalesBean? = mFormalityEntity.Prospecto?.Firma?.DatosAdicionales
        if contactoBean != nil {
            mNameTextField.text = contactoBean?.Nombre.uppercased()
            mLastNameTextField.text = contactoBean?.ApellidoPaterno.uppercased()
            mMotherLastNameTextField.text = contactoBean?.ApellidoMaterno.uppercased()
            mBirthdayTextField.text = contactoBean?.FechaNacimiento
            mEmailTextField.text = contactoBean?.CorreoElectronico.uppercased()
            mHomePhoneTextField.text = contactoBean?.Telefono
            mCelPhoneTextField.text = contactoBean?.Celular
            mRfcTextField.text = contactoBean?.RFC.uppercased()
            mContactTextField.text = contactoBean?.MedioContacto.uppercased()
            mBusinessNameTextField.text = contactoBean?.RazonSocial.uppercased()
            if contactoBean?.TipoPersona == "Moral" {
                mPersonTypeSegmentedControl.selectedSegmentIndex = 1
                ViewUtils.setVisibility(view: mBusinessNameView, visibility: .NOT_GONE, controller : self, animated : true)
            }
        }
        if infoAdicionalBean != nil {
            mIdentificationTypeTextField.text = infoAdicionalBean?.TipoIdentificacion
            mIdentificationNumberTextField.text = infoAdicionalBean?.IdentificacionOficial
        }
    }
    
    override func getPresenter() -> BasePresenter? {
        mPersonDataPresenter = PersonDataPresenter(viewController : self)
        return mPersonDataPresenter
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    @IBAction func mPersonTypeSegmentedControl(_ sender: UISegmentedControl) {
        switch sender.selectedSegmentIndex{
        case 0:
            ViewUtils.setVisibility(view: mBusinessNameView, visibility: .GONE, controller: self, animated : true)
            mTagBornLabel.text = "Fecha de nacimiento"
            mTagPhoneHouseLabel.text = "Teléfono de casa"
        case 1:
            ViewUtils.setVisibility(view: mBusinessNameView, visibility: .NOT_GONE, controller : self, animated : true)
            mTagBornLabel.text = "Fecha de constitución"
            mTagPhoneHouseLabel.text = "Teléfono de oficina"
        default:
            break;
        }
    }
    
    func viewControllerForResult(keyRequest: String, result: ViewControllerResult, data: [String : AnyObject]) {
        if keyRequest == ContractingViewController.REQUEST_CODE_ADD_ADDRESS_INFO {
            if result == ViewControllerResult.RESULT_OK {
                resultValue = .RESULT_OK
                resultDelegate?.viewControllerForResult(keyRequest: requestValue, result: resultValue, data: data)
            }
        }
    }
    
    @IBAction func onNextButtonClick(_ sender: Any) {
        if mFormValidator.isValid() {
            let contactoBean : ContactoBean = ContactoBean()
            contactoBean.Nombre = (mNameTextField.text?.trim())!
            contactoBean.ApellidoPaterno = (mLastNameTextField.text?.trim())!
            contactoBean.ApellidoMaterno = (mMotherLastNameTextField.text?.trim())!
            contactoBean.FechaNacimiento = (mBirthdayTextField.text?.trim())!
            contactoBean.RFC = (mRfcTextField.text?.trim())!
            contactoBean.CorreoElectronico = (mEmailTextField.text?.trim())!
            contactoBean.Celular = (mCelPhoneTextField.text?.trim())!
            contactoBean.Telefono = (mHomePhoneTextField.text?.trim())!
            contactoBean.RazonSocial = (mBusinessNameTextField.text?.trim())!
            contactoBean.MedioContacto = (mContactTextField.text?.trim())!
            if mPersonTypeSegmentedControl.selectedSegmentIndex == 0 {
                contactoBean.TipoPersona = ("Física");
                contactoBean.RazonSocial = ""
            } else {
                contactoBean.TipoPersona = ("Moral");
            }

            let infoAdicionalBean : DatosAdicionalesBean = DatosAdicionalesBean()
            infoAdicionalBean.TipoIdentificacion = (mIdentificationTypeTextField.text?.trim())!
            infoAdicionalBean.IdentificacionOficial = (mIdentificationNumberTextField.text?.trim())!
            mPersonDataPresenter.saveContactoBean(contactoBean: contactoBean, infoAdicionalBean: infoAdicionalBean)
            ViewControllerUtils.pushViewControllerWithResult(from: self, to: AddressDataViewController.self, request: ContractingViewController.REQUEST_CODE_ADD_ADDRESS_INFO)
        } else {
            AlertDialog.show(title: "Error", body: StringDialogs.dialog_error_empty_fields_new, view: self)
        }
    }
    
    
}

