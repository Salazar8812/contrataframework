//
//  PackagesViewController.swift
//  EstrategiaDigital
//
//  Created by Charls Salazar on 05/07/17.
//  Copyright © 2017 Charls Salazar. All rights reserved.
//

import UIKit

class PackagesViewController: BaseViewController, FamilyPackageDelegate, CarruselDataDelegate {
    
    @IBOutlet weak var listPackageCarousel: iCarousel!
    
    var descriptionArray = [DescriptionPackage]()
    var selectedIndex : Int!
    var indice : Int!
    var descriptionPDataSoure : DescriptionPackageDataSource?
    var mCarruselDataSource : CarruselDataSource?
    var mFamilyPackagePresenter : FamilyPackagesPresenter?
    @IBOutlet weak var mTitleFamilyPackage: UILabel!
    @IBOutlet weak var mDetailPlanLabel: UILabel!
    var mArrayFamily: [Plans] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        mCarruselDataSource = CarruselDataSource(carrusel: listPackageCarousel, mCarruselDataDelegate: self)
        mCarruselDataSource?.settings()
        
        mFamilyPackagePresenter?.loadFamilyPackage()
    }
    
    override func getPresenter() -> BasePresenter? {
        mFamilyPackagePresenter = FamilyPackagesPresenter(viewController: self, mFamilyPackageDelegate: self)
        return mFamilyPackagePresenter
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    
    func onSuccessFamilyPackage(familyPackageResponse: [Plans]) {
        if familyPackageResponse.count > 0 {
            mCarruselDataSource?.update(items:familyPackageResponse)
            mArrayFamily = familyPackageResponse
            mTitleFamilyPackage.text = mArrayFamily[0].planName
            mDetailPlanLabel.text = mArrayFamily[0].planDetail
        }
    }
    
    func onItemClick(planSelected: Plans) {
        var extras : [String : AnyObject] = [:]
        extras[KeysEnum.EXTRA_PLAN] = planSelected
        ViewControllerUtils.pushViewController(from: self, to: AdditionalServiceViewController.self, extras: extras)
    }
    
    func onChangeItem(planDetail: String, planName: String) {
        mTitleFamilyPackage.text = planName
        mDetailPlanLabel.text = planDetail
    }
    
}

