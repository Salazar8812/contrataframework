//
//  CapturePhotoViewController.swift
//  EstrategiaDigital
//
//  Created by Charls Salazar on 30/10/17.
//  Copyright © 2017 Charls Salazar. All rights reserved.
//

import UIKit
import IPDFCameraViewController

class CapturePhotoViewController: BaseViewController {
    
    @IBOutlet weak var mCameraView: IPDFCameraViewController!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        mCameraView.setupCameraView()
        mCameraView.isBorderDetectionEnabled = true
    }

    override func viewDidAppear(_ animated: Bool) {
        mCameraView.start()
        mCameraView.cameraViewType = IPDFCameraViewType.normal
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    @IBAction func mCaptureButton(_ sender: Any) {
        mCameraView.captureImage(completionHander: { (imagefile) -> Void in
            self.resultValue = .RESULT_OK
            self.data[KeysEnum.EXTRA_CAPTURE_IMAGE] = imagefile as AnyObject
            ViewControllerUtils.popViewController(viewController: self)
        })
    }
}
