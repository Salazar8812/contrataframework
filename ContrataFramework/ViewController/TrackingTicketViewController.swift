//
//  TrackingTicketViewController.swift
//  EstrategiaDigital
//
//  Created by Charls Salazar on 30/10/17.
//  Copyright © 2017 Charls Salazar. All rights reserved.
//

import UIKit

class TrackingTicketViewController: BaseEstrategiaViewController {
    
    @IBOutlet weak var mAdicionalesTableView: UITableView!
    @IBOutlet weak var mDateLabel: UILabel!
    @IBOutlet weak var mNumberOfTicketLabel: UILabel!
    @IBOutlet weak var mListAddonsTableView: UITableView!
    @IBOutlet weak var mPackageNameLabel: UILabel!
    @IBOutlet weak var mTotalLabel: UILabel!
    
    var mAdicionalesDataSource : BaseDataSource<NameData, AdicionalCell>!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        clearData()
        
        mTotalLabel.text = String(format: "$%.2f", ceil(mFormalityEntity.totalPriceList))
        
        mNumberOfTicketLabel.text = mFormalityEntity.Prospecto?.CuentaBRM
        mPackageNameLabel.text = mFormalityEntity.Prospecto?.Propuesta?.Cot_SitioPlan?.NombrePlan
        mDateLabel.text = DateUtils.getDateNow()
        
        mAdicionalesDataSource = BaseDataSource(tableView: mAdicionalesTableView)

        mAdicionalesDataSource.update(items: Array(mFormalityEntity.addonsServices))
    }
    
    func clearData() {
        mNumberOfTicketLabel.text = ""
        mPackageNameLabel.text = ""
        mTotalLabel.text = ""
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    @IBAction func mSaveButton(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
}
