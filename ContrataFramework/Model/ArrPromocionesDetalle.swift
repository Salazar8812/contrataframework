
//
//  ArrServicesAdditionals.swift
//  VentasTotalPlayiOS
//
//  Created by Daniel García Aldynamic varado on 12/20/16.
//  Copyright © 2016 TotalPlay. All rights reserved.
//

import UIKit
import ObjectMapper
import RealmSwift

public class ArrPromocionesDetalle  : Object, Mappable {

    @objc public dynamic var uuid : String = UUID().uuidString
	@objc public dynamic var id : String = ""
	@objc public dynamic var nombre : String = ""
	@objc public dynamic var cATCanalVenta : String = ""
	@objc public dynamic var cATCiudad : String = ""
	@objc public dynamic var cATCluster : String = ""
	@objc public dynamic var cATPlazo : String = ""
	@objc public dynamic var codigoPostal : String = ""
	@objc public dynamic var cATTipoPago : String = ""
	@objc public dynamic var dPPromocion : String = ""
	@objc public dynamic var estatus : String = ""

	required convenience public init?(map: Map) {
        self.init()
    }

    public func mapping(map: Map) {
         
		id		<- map["Id"]
		nombre		<- map["Nombre"]
		cATCanalVenta		<- map["CAT_CanalVenta"]
		cATCiudad		<- map["CAT_Ciudad"]
		cATCluster		<- map["CAT_Cluster"]
		cATPlazo		<- map["CAT_Plazo"]
		codigoPostal		<- map["CodigoPostal"]
		cATTipoPago		<- map["CAT_TipoPago"]
		dPPromocion		<- map["DP_Promocion"]
		estatus		<- map["Estatus"] 
	}

    override public static func primaryKey() -> String? {
        return "uuid"
    }
    
}
