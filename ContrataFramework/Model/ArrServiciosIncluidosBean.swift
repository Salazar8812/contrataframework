//
//  ArrServiciosIncluidosBean.swift
//  EstrategiaDigital
//
//  Created by Charls Salazar on 25/10/17.
//  Copyright © 2017 Charls Salazar. All rights reserved.
//

import UIKit
import ObjectMapper

public class ArrServiciosIncluidosBean: NSObject {
    
    @objc public var Id : String?
    @objc public var Nombre : String?
    @objc public var CantidadEquiposAutomaticos : String?
    @objc public var Comentario : String?
    @objc public var MaximoAgregar : String?
    @objc public var Nombrecomercial : String?
    @objc public var NombreEditable : String?
    @objc public var PlanId : String?
    @objc public var Precio : String?
    @objc public var SeActiva : String?
    @objc public var SeFactura : String?
    @objc public var ServicioId : String?
    @objc public var SRV_Mode : String?
    @objc public var TipoServicio : String?
    @objc public var TipoTelefonia : String?
    @objc public var ArrProductosIncluidos : [ArrProductosIncluidosBean] = []
    
}
