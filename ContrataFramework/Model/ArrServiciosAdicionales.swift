
//
//  ArrServicesAdditionals.swift
//  VentasTotalPlayiOS
//
//  Created by Daniel García Aldynamic varado on 12/20/16.
//  Copyright © 2016 TotalPlay. All rights reserved.
//

import UIKit
import ObjectMapper

public class ArrServiciosAdicionales  : NSObject, Mappable, AdicionalesBean {
    
    public func getID() -> String {
        return id
    }
    
    public func getAdicionalType() -> Int {
        return AddicionalBeanType.SERVICE
    }
    
    public func getAdicionalName() -> String {
        return nombre
    }
    
    public func getPrecioBase() -> Double {
        return 0
    }
    public func getPrecioProntoPago() -> Double {
        return 0
    }
    public func getCantidad() -> Double {
        return 0
    }
    public func getIVA() -> Double {
        return 0
    }
    public func getIEPS() -> Double {
        return 0
    }

    @objc public dynamic var uuid : String = UUID().uuidString
	@objc public dynamic var id : String = ""
	@objc public dynamic var nombre : String = ""
	@objc public dynamic var cantidadEquiposAutomaticos : String = ""
	@objc public dynamic var comentario : String = ""
	@objc public dynamic var maximoAgregar : String = ""
	@objc public dynamic var nombrecomercial : String = ""
	@objc public dynamic var nombreEditable : String = ""
	@objc public dynamic var planId : String = ""
	@objc public dynamic var precio : String = ""
	@objc public dynamic var seActiva : String = ""
	@objc public dynamic var seFactura : String = ""
	@objc public dynamic var servicioId : String = ""
	@objc public dynamic var sRVMode : String = ""
	@objc public dynamic var tipoServicio : String = ""
	@objc public dynamic var tipoTelefonia : String = ""
    @objc public dynamic var priceApp : String = ""
    @objc public dynamic var image : String = ""
    public var arrProductosIncluidos : [ArrProductosIncluido] = []

	required convenience public init?(map: Map) {
        self.init()
    }

    public func mapping(map: Map) {
        id		<- map["Id"]
		nombre		<- map["Nombre"]
		cantidadEquiposAutomaticos		<- map["CantidadEquiposAutomaticos"]
		comentario		<- map["Comentario"]
		maximoAgregar		<- map["MaximoAgregar"]
		nombrecomercial		<- map["Nombrecomercial"]
		nombreEditable		<- map["NombreEditable"]
		planId		<- map["PlanId"]
		precio		<- map["Precio"]
		seActiva		<- map["SeActiva"]
		seFactura		<- map["SeFactura"]
		servicioId		<- map["ServicioId"]
		sRVMode		<- map["SRV_Mode"]
		tipoServicio		<- map["TipoServicio"]
		tipoTelefonia		<- map["TipoTelefonia"]
        priceApp	<- map["priceApp"]
        image    <- map["ImgIconoApp"]
        
        arrProductosIncluidos    <- map["ArrProductosIncluidos"]
	}


}
