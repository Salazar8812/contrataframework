
//
//  ArrServicesAdditionals.swift
//  VentasTotalPlayiOS
//
//  Created by Daniel García Aldynamic varado on 12/20/16.
//  Copyright © 2016 TotalPlay. All rights reserved.
//

import UIKit
import ObjectMapper
import RealmSwift

public class ArrPromocionesPlan  : Object, Mappable {

    @objc public dynamic var uuid : String = UUID().uuidString
	@objc public  dynamic var id : String = ""
	@objc public dynamic var nombre : String = ""
	@objc public dynamic var comentario : String = ""
	@objc public dynamic var descripcion : String = ""
	@objc public dynamic var plan : String = ""
	@objc public dynamic var planCambioParrilla : String = ""
	@objc public dynamic var promocion : String = ""
	@objc public dynamic var estatus : String = ""
	@objc public dynamic var plazo : String = ""
	@objc public dynamic var estatusPromociones : String = ""
	@objc public dynamic var comentarioPromocion : String = ""
	@objc public dynamic var nombrePromocion : String = ""
	@objc public dynamic var companiaId : String = ""
	@objc public dynamic var esAutomatica : String = ""
	@objc public dynamic var esPostVenta : String = ""
	@objc public dynamic var estatusPromocion : String = ""
	@objc public dynamic var esVentaNueva : String = ""
	@objc public dynamic var fechaFin : String = ""
	@objc public dynamic var fechaInicio : String = ""
	@objc public dynamic var idPromocion : String = ""
    public var arrPromocionesConvivencia : List<ArrPromocionesConvivencium> = List<ArrPromocionesConvivencium>()
    public var arrPromocionesDetalle : List<ArrPromocionesDetalle> = List<ArrPromocionesDetalle>()
    public var arrDPPromocionesServicioProducto : List<ArrDPPromocionesServicioProducto> = List<ArrDPPromocionesServicioProducto>()

    public var isSelected : Bool = false
    
	required convenience public init?(map: Map) {
        self.init()
    }

    public func mapping(map: Map) {
         
		id		<- map["Id"]
		nombre		<- map["Nombre"]
		comentario		<- map["Comentario"]
		descripcion		<- map["Descripcion"]
		plan		<- map["Plan"]
		planCambioParrilla		<- map["PlanCambioParrilla"]
		promocion		<- map["Promocion"]
		estatus		<- map["Estatus"]
		plazo		<- map["Plazo"]
		estatusPromociones		<- map["EstatusPromociones"]
		comentarioPromocion		<- map["ComentarioPromocion"]
		nombrePromocion		<- map["NombrePromocion"]
		companiaId		<- map["CompaniaId"]
		esAutomatica		<- map["EsAutomatica"]
		esPostVenta		<- map["EsPostVenta"]
		estatusPromocion		<- map["EstatusPromocion"]
		esVentaNueva		<- map["EsVentaNueva"]
		fechaFin		<- map["FechaFin"]
		fechaInicio		<- map["FechaInicio"]
		idPromocion		<- map["IdPromocion"]
        
        let objArrPromocionesConvivencia = List<ArrPromocionesConvivencium>()
        for item in arrPromocionesConvivencia{
            objArrPromocionesConvivencia.append(ArrPromocionesConvivencium(value: item as Any))
        }
        self.arrPromocionesConvivencia = objArrPromocionesConvivencia
		self.arrPromocionesConvivencia   <- map["ArrPromocionesConvivencia"]
        
        let objArrPromocionesDetalle = List<ArrPromocionesDetalle>()
        for item in arrPromocionesDetalle{
            objArrPromocionesDetalle.append(ArrPromocionesDetalle(value: item as Any))
        }
        self.arrPromocionesDetalle = objArrPromocionesDetalle
		self.arrPromocionesDetalle		<- map["ArrPromocionesDetalle"]
        
        let objArrDPPromocionesServicioProducto = List<ArrDPPromocionesServicioProducto>()
        for item in arrDPPromocionesServicioProducto{
            objArrDPPromocionesServicioProducto.append(ArrDPPromocionesServicioProducto(value: item as Any))
        }
        self.arrDPPromocionesServicioProducto = objArrDPPromocionesServicioProducto
		self.arrDPPromocionesServicioProducto 		<- map["ArrDP_PromocionesServicioProducto"]
        
	}
    
    override public static func primaryKey() -> String? {
        return "uuid"
    }

}
