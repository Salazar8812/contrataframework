
//
//  ArrServicesAdditionals.swift
//  VentasTotalPlayiOS
//
//  Created by Daniel García Aldynamic varado on 12/20/16.
//  Copyright © 2016 TotalPlay. All rights reserved.
//

import UIKit
import ObjectMapper
import RealmSwift

public class ReferidoBean  : Object, Mappable {

    @objc public dynamic var uuid : String = UUID().uuidString
	@objc public dynamic var Nombre : String = ""
	@objc public dynamic var ApellidoPaterno : String = ""
	@objc public dynamic var ApellidoMaterno : String = ""
	@objc public dynamic var Parentesco : String = ""
	@objc public dynamic var Telefono : String = ""
    
    public var isValid : Bool = false

	required public convenience init?(map: Map) {
        self.init()
    }
    
    

    public func mapping(map: Map) {
         
		Nombre		<- map["Nombre"] 
		ApellidoPaterno		<- map["ApellidoPaterno"] 
		ApellidoMaterno		<- map["ApellidoMaterno"] 
		Parentesco		<- map["Parentesco"] 
		Telefono		<- map["Telefono"] 
	}
    
    override public static func primaryKey() -> String? {
        return "uuid"
    }

}
