
//
//  ArrServicesAdditionals.swift
//  VentasTotalPlayiOS
//
//  Created by Daniel García Aldynamic varado on 12/20/16.
//  Copyright © 2016 TotalPlay. All rights reserved.
//

import UIKit
import ObjectMapper
import RealmSwift

public class ArrPromocionesConvivencium  : Object, Mappable {

    @objc public dynamic var uuid : String = UUID().uuidString
	@objc public dynamic var id : String = ""
	@objc public dynamic var nombre : String = ""
	@objc public dynamic var estatus : String = ""
	@objc public dynamic var promocionPlanId : String = ""
	@objc public dynamic var promocionExcId : String = ""

	required convenience public init?(map: Map) {
        self.init()
    }

    public func mapping(map: Map) {
         
		id		<- map["Id"]
		nombre		<- map["Nombre"]
		estatus		<- map["Estatus"]
		promocionPlanId		<- map["PromocionPlanId"]
		promocionExcId		<- map["PromocionExcId"]
	}

    override public static func primaryKey() -> String? {
        return "uuid"
    }
    
}
