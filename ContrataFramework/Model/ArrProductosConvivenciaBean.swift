
//
//  ArrServicesAdditionals.swift
//  VentasTotalPlayiOS
//
//  Created by Daniel García Aldynamic varado on 12/20/16.
//  Copyright © 2016 TotalPlay. All rights reserved.
//

import UIKit
import ObjectMapper 
import RealmSwift

public class ArrProductosConvivenciaBean  : Object, Mappable {

    @objc public dynamic var uuid : String = UUID().uuidString
	@objc public dynamic var Id : String = ""
	@objc public dynamic var Name : String = ""
	@objc public dynamic var Comentario : String = ""
	@objc public dynamic var Estatus : String = ""
	@objc public dynamic var ProductoId : String = ""
	@objc public dynamic var ProductoExcIncId : String = ""
	@objc public dynamic var Tipo : String = ""

	required public convenience init?(map: Map) {
        self.init()
    }

    public func mapping(map: Map) {
         
		Id		<- map["Id"] 
		Name		<- map["Name"] 
		Comentario		<- map["Comentario"] 
		Estatus		<- map["Estatus"] 
		ProductoId		<- map["ProductoId"] 
		ProductoExcIncId		<- map["ProductoExcIncId"] 
		Tipo		<- map["Tipo"] 
	}
    
    override public static func primaryKey() -> String? {
        return "uuid"
    }


}
