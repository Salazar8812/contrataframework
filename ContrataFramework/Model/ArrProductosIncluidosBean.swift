
//
//  ArrProductosIncluidosBean.swift
//  EstrategiaDigital
//
//  Created by Charls Salazar on 25/10/17.
//  Copyright © 2017 Charls Salazar. All rights reserved.
//

import UIKit

public class ArrProductosIncluidosBean: NSObject {
    @objc public var Id : String?
    @objc public var Nombre : String?
    @objc public var AgrupacionAddon: String?
    @objc public var CantidadDN : String?
    @objc public var CantidadTroncal : String?
    @objc public var Comentario : String?
    @objc public var EsAutomaticoCiudad : String?
    @objc public var EsCargoUnico : String?
    @objc public var Estatus : String?
    @objc public var EsVisible : String?
    @objc public var IdBrmArrear : String?
    @objc public var IdBrmCU : String?
    @objc public var IdBrmForward : String?
    @objc public var IEPS : String?
    @objc public var IVA : String?
    @objc public var MaximoAgregar : String?
    @objc public var NombreEditable : String?
    @objc public var PlanDescuentoId : String?
    @objc public var Plazo : String?
    @objc public var PrecioBase : String?
    @objc public var PrecioEditable : String?
    @objc public var PrecioProntoPago : String?
    @objc public var ProductoPadre : String?
    @objc public var TipoProducto : String?
    @objc public var IdProducto : String?
    @objc public var NameProducto : String?
    @objc public var ProductoId : String?
    @objc public var Ciudad : String?
    @objc public var VelocidadSubida : String?
    @objc public var VelocidadBajada : String?
    @objc public var TieneIPDinamica : String?
    @objc public var TieneIPFija : String?
    @objc public var TieneSTBAdicional : String?
    @objc public var EsCCTV : String?
    @objc public var EsWiFi : String?
    @objc public var Cantidad : String?
    @objc public var EstatusProducto : String?
    @objc public var FechaInicio : String?
    @objc public var FechaFin : String?
    @objc public var ComentarioProducto : String?
    @objc public var EsProntoPago : String?
}
