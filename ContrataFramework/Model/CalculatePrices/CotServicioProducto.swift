//
//  CotServicioProducto.swift
//  EstrategiaDigital
//
//  Created by Jorge Hdez VIlla on 08/11/17.
//  Copyright © 2017 Charls Salazar. All rights reserved.
//

import UIKit

class CotServicioProducto: NSObject {
    
    var nombre : String = ""
    var precioUnitarioBase : Double = 0
    var precioUnitarioProntoPago : Double = 0
    var precioUnitario : Double = 0
    var cantidad : Int = 0
    var descuento : Double = 0
    var impuesto1 : Double = 0
    var impuesto2 : Double = 0
    var tipoProducto : Int = 0
    var esCargoUnico : Bool = false
    var esProntoPago : Bool = false
    var esProductoAdicional : Bool = false
    


    init(producto : AdicionalesBean) {
        precioUnitario = producto.getPrecioBase()
        precioUnitarioBase = producto.getPrecioBase()
        precioUnitarioProntoPago = producto.getPrecioProntoPago()
        cantidad = Int(producto.getCantidad())
        impuesto1 = producto.getIVA()
        impuesto2 = producto.getIEPS()
        tipoProducto = producto.getAdicionalType()
    }

}
