//
//  CotSitioPlan.swift
//  EstrategiaDigital
//
//  Created by Jorge Hdez VIlla on 08/11/17.
//  Copyright © 2017 Charls Salazar. All rights reserved.
//

import UIKit

class CotSitioPlan: NSObject {
    
    var cotServicioProductoList : [CotServicioProducto] = []
    var descuentoRenta : Double = 0
    var descuentoCargoUnico : Double = 0
    var doPlan : Double = 0
    
}
