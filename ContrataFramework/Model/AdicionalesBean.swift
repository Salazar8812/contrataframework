//
//  AdicionalesBean.swift
//  EstrategiaDigital
//
//  Created by Charls Salazar on 26/10/17.
//  Copyright © 2017 Charls Salazar. All rights reserved.
//

import UIKit

class AddicionalBeanType {
    static var SERVICE : Int = 1
    static var PRODUCT_INCLUYED : Int = 2
    static var PRODUCT_ADDITIONAL : Int = 3
}
public protocol AdicionalesBean: NSObjectProtocol {
    func getAdicionalType() -> Int
    func getID() -> String
    func getAdicionalName() -> String
    
    func getPrecioBase() -> Double
    func getPrecioProntoPago() -> Double
    func getCantidad() -> Double
    func getIVA() -> Double
    func getIEPS() -> Double
}
