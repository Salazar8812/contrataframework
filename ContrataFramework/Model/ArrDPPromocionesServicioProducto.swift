
//
//  ArrServicesAdditionals.swift
//  VentasTotalPlayiOS
//
//  Created by Daniel García Aldynamic varado on 12/20/16.
//  Copyright © 2016 TotalPlay. All rights reserved.
//

import UIKit
import ObjectMapper
import RealmSwift

public class ArrDPPromocionesServicioProducto  : Object, Mappable {

    @objc public dynamic var uuid : String = UUID().uuidString
	@objc public dynamic var id : String = ""
	@objc public dynamic var nombre : String = ""
	@objc public dynamic var agregarServicio : String = ""
	@objc public dynamic var esCargoUnico : String = ""
	@objc public dynamic var estatus : String = ""
	@objc public dynamic var iEPS : String = ""
	@objc public dynamic var iVA : String = ""
	@objc public dynamic var mesInicio : String = ""
	@objc public dynamic var montoC : String = ""
	@objc public dynamic var planServicio : String = ""
	@objc public dynamic var porcentaje : String = ""
	@objc public dynamic var productoCaracteristica : String = ""
	@objc public dynamic var promocionPlan : String = ""
	@objc public dynamic var servicioProducto : String = ""
	@objc public dynamic var vigenciaMes : String = ""
	@objc public dynamic var idProductoC : String = ""
	@objc public dynamic var nameProductoC : String = ""
	@objc public dynamic var productoId : String = ""
	@objc public dynamic var ciudad : String = ""
	@objc public dynamic var velocidadSubida : String = ""
	@objc public dynamic var velocidadBajada : String = ""
	@objc public dynamic var tieneIPDinamica : String = ""
	@objc public dynamic var tieneIPFija : String = ""
	@objc public dynamic var tieneSTBAdicional : String = ""
	@objc public dynamic var esCCTV : String = ""
	@objc public dynamic var esWiFi : String = ""
	@objc public dynamic var cantidad : String = ""
	@objc public dynamic var estatusProductoC : String = ""
	@objc public dynamic var fechaInicio : String = ""
	@objc public dynamic var fechaFin : String = ""
	@objc public dynamic var comentario : String = ""
	@objc public dynamic var esProntoPago : String = ""
	@objc public dynamic var dPServicioProducto : DPServicioProducto? = DPServicioProducto()

	required convenience public init?(map: Map) {
        self.init()
    }

    public func mapping(map: Map) {
         
		id		<- map["Id"]
		nombre		<- map["Nombre"]
		agregarServicio		<- map["AgregarServicio"]
		esCargoUnico		<- map["EsCargoUnico"]
		estatus		<- map["Estatus"]
		iEPS		<- map["IEPS"]
		iVA		<- map["IVA"]
		mesInicio		<- map["MesInicio"]
		montoC		<- map["Monto__c"]
		planServicio		<- map["PlanServicio"]
		porcentaje		<- map["Porcentaje"]
		productoCaracteristica		<- map["ProductoCaracteristica"]
		promocionPlan		<- map["PromocionPlan"]
		servicioProducto		<- map["ServicioProducto"]
		vigenciaMes		<- map["VigenciaMes"]
		idProductoC		<- map["IdProductoC"]
		nameProductoC		<- map["NameProductoC"]
		productoId		<- map["ProductoId"]
		ciudad		<- map["Ciudad"]
		velocidadSubida		<- map["VelocidadSubida"]
		velocidadBajada		<- map["VelocidadBajada"]
		tieneIPDinamica		<- map["TieneIPDinamica"]
		tieneIPFija		<- map["TieneIPFija"]
		tieneSTBAdicional		<- map["TieneSTBAdicional"]
		esCCTV		<- map["EsCCTV"]
		esWiFi		<- map["EsWiFi"]
		cantidad		<- map["Cantidad"]
		estatusProductoC		<- map["EstatusProductoC"]
		fechaInicio		<- map["FechaInicio"]
		fechaFin		<- map["FechaFin"]
		comentario		<- map["Comentario"]
		esProntoPago		<- map["EsProntoPago"]
        
        self.dPServicioProducto = DPServicioProducto(value: dPServicioProducto as Any)
		self.dPServicioProducto		<- map["DP_ServicioProducto"]
	}
    
    override public static func primaryKey() -> String? {
        return "uuid"
    }

}
