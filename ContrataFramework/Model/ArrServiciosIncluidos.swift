
//
//  ArrServicesAdditionals.swift
//  VentasTotalPlayiOS
//
//  Created by Daniel García Aldynamic varado on 12/20/16.
//  Copyright © 2016 TotalPlay. All rights reserved.
//

import UIKit
import ObjectMapper

public class ArrServiciosIncluidos  : NSObject, Mappable {
    
    @objc public var uuid : String = UUID().uuidString
	@objc public var id : String = ""
	@objc public var nombre : String = ""
	@objc public var cantidadEquiposAutomaticos : String = ""
	@objc public var comentario : String = ""
	@objc public var maximoAgregar : String = ""
	@objc public var nombrecomercial : String = ""
	@objc public var nombreEditable : String = ""
	@objc public var planId : String = ""
	@objc public var precio : String = ""
	@objc public var seActiva : String = ""
	@objc public var seFactura : String = ""
	@objc public var servicioId : String = ""
	@objc public var sRVMode : String = ""
	@objc public var tipoServicio : String = ""
	@objc public var tipoTelefonia : String = ""
    @objc public var arrProductosIncluidos : [ArrProductosIncluido] = []

	required convenience public init?(map: Map) {
        self.init()
    }

    public func mapping(map: Map) {
         
		id		<- map["Id"]
		nombre		<- map["Nombre"]
		cantidadEquiposAutomaticos		<- map["CantidadEquiposAutomaticos"]
		comentario		<- map["Comentario"]
		maximoAgregar		<- map["MaximoAgregar"]
		nombrecomercial		<- map["Nombrecomercial"]
		nombreEditable		<- map["NombreEditable"]
		planId		<- map["PlanId"]
		precio		<- map["Precio"]
		seActiva		<- map["SeActiva"]
		seFactura		<- map["SeFactura"]
		servicioId		<- map["ServicioId"]
		sRVMode		<- map["SRV_Mode"]
		tipoServicio		<- map["TipoServicio"]
		tipoTelefonia		<- map["TipoTelefonia"]
        
        self.arrProductosIncluidos         <- map["ArrProductosIncluidos"]
	}

}
