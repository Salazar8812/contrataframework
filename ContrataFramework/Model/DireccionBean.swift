
//
//  ArrServicesAdditionals.swift
//  VentasTotalPlayiOS
//
//  Created by Daniel García Aldynamic varado on 12/20/16.
//  Copyright © 2016 TotalPlay. All rights reserved.
//

import UIKit
import ObjectMapper
import RealmSwift

public class DireccionBean  : Object, Mappable {

    @objc public dynamic var uuid : String = UUID().uuidString
	@objc public dynamic var zipCode : String = ""
	@objc public dynamic var colony : String = ""
	@objc public dynamic var delegation : String = ""
	@objc public dynamic var city : String = ""
	@objc public dynamic var state : String = ""
	@objc public dynamic var street : String = ""
	@objc public dynamic var noExt : String = ""
	@objc public dynamic var noInt : String = ""
	@objc public dynamic var betweenStreets : String = ""
	@objc public dynamic var ReferenciaUrbana : String = ""
	@objc public dynamic var authorizationWhitoutCoverage : String = ""
	@objc public dynamic var cluster : String = ""
	@objc public dynamic var place : String = ""
	@objc public dynamic var district : String = ""
	@objc public dynamic var feasibility : String = ""
	@objc public dynamic var feasible : String = ""
	@objc public dynamic var typeCoverage : String = ""
	@objc public dynamic var region : String = ""
	@objc public dynamic var idRegion : String = ""
    @objc public dynamic var zoneValue : String = ""
    @objc public dynamic var latitude : String = ""
    @objc public dynamic var longitude : String = ""
    
    
	required convenience public init?(map: Map) {
        self.init()
    }

    public func mapping(map: Map) {
         
		zipCode		<- map["CodigoPostal"]
		colony		<- map["Colonia"]
		delegation		<- map["Delegacion"]
		city		<- map["Ciudad"]
		state		<- map["Estado"]
		street		<- map["Calle"]
		noExt		<- map["NumeroExterior"]
		noInt		<- map["NumeroInterior"]
		betweenStreets		<- map["EntreCalles"]
		ReferenciaUrbana		<- map["ReferenciaUrbana"] 
		authorizationWhitoutCoverage		<- map["AutorizacionSinCobertura"]
		cluster		<- map["Cluster"]
		place		<- map["Plaza"]
		district		<- map["Distrito"]
		feasibility		<- map["Factibilidad"]
		feasible		<- map["Factible"]
		typeCoverage		<- map["TipoCobertura"]
		region		<- map["Region"]
		idRegion		<- map["RegionId"]
        zoneValue		<- map["zona"]
        latitude		<- map["Latitude"]
        longitude		<- map["Longitude"]
	}
    
    override public static func primaryKey() -> String? {
        return "uuid"
    }

}
