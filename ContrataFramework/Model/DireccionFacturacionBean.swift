
//
//  ArrServicesAdditionals.swift
//  VentasTotalPlayiOS
//
//  Created by Daniel García Aldynamic varado on 12/20/16.
//  Copyright © 2016 TotalPlay. All rights reserved.
//

import UIKit
import ObjectMapper
import RealmSwift

public class DireccionFacturacionBean : Object, Mappable {

    @objc public dynamic var uuid : String = UUID().uuidString
	@objc public dynamic var MismaDireccionInstalacion : String = ""
	@objc public dynamic var FolioContrato : String = ""
	@objc public dynamic var Calle : String = ""
	@objc public dynamic var NumeroExterior : String = ""
	@objc public dynamic var NumeroInterior : String = ""
	@objc public dynamic var ColoniaCP : String = ""
	@objc public dynamic var Colonia : String = ""
	@objc public dynamic var DelegacionMunicipio : String = ""
	@objc public dynamic var Ciudad : String = ""
	@objc public dynamic var Estado : String = ""
	@objc public dynamic var CodigoPostal : String = ""


	required convenience public init?(map: Map) {
        self.init()
    }

    public func mapping(map: Map) {
         
		MismaDireccionInstalacion		<- map["MismaDireccionInstalacion"] 
		FolioContrato		<- map["FolioContrato"] 
		Calle		<- map["Calle"] 
		NumeroExterior		<- map["NumeroExterior"] 
		NumeroInterior		<- map["NumeroInterior"] 
		ColoniaCP		<- map["ColoniaCP"] 
		Colonia		<- map["Colonia"] 
		DelegacionMunicipio		<- map["DelegacionMunicipio"] 
		Ciudad		<- map["Ciudad"] 
		Estado		<- map["Estado"] 
		CodigoPostal		<- map["CodigoPostal"] 
	}
    
    override public static func primaryKey() -> String? {
        return "uuid"
    }

}
