
//
//  ArrServicesAdditionals.swift
//  VentasTotalPlayiOS
//
//  Created by Daniel García Aldynamic varado on 12/20/16.
//  Copyright © 2016 TotalPlay. All rights reserved.
//

import UIKit
import ObjectMapper
import RealmSwift

public class CotSitioPlanBean  : Object, Mappable {

    @objc public dynamic var uuid : String = UUID().uuidString
	@objc public dynamic var DescuentoCargoUnico : String = ""
	@objc public dynamic var DescuentoRenta : String = ""
	@objc public dynamic var DN_Principal : String = ""
	@objc public dynamic var DP_Plan : String = ""
	@objc public dynamic var DP_DescuentoLimiteCargoUnico : String = ""
	@objc public dynamic var DP_DescuentoLimiteRenta : String = ""
	@objc public dynamic var EstatusActivacion : String = ""
	@objc public dynamic var NombrePlan : String = ""
	@objc public dynamic var PrecioProntoPago : String = ""
	@objc public dynamic var Ticket : String = ""

	required convenience public init?(map: Map) {
        self.init()
    }

    public func mapping(map: Map) {
         
		DescuentoCargoUnico		<- map["DescuentoCargoUnico"] 
		DescuentoRenta		<- map["DescuentoRenta"] 
		DN_Principal		<- map["DN_Principal"] 
		DP_Plan		<- map["DP_Plan"] 
		DP_DescuentoLimiteCargoUnico		<- map["DP_DescuentoLimiteCargoUnico"] 
		DP_DescuentoLimiteRenta		<- map["DP_DescuentoLimiteRenta"] 
		EstatusActivacion		<- map["EstatusActivacion"] 
		NombrePlan		<- map["NombrePlan"] 
		PrecioProntoPago		<- map["PrecioProntoPago"] 
		Ticket		<- map["Ticket"] 
	}

    override public static func primaryKey() -> String? {
        return "uuid"
    }
    
}
