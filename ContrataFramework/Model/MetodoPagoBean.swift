
//
//  ArrServicesAdditionals.swift
//  VentasTotalPlayiOS
//
//  Created by Daniel García Aldynamic varado on 12/20/16.
//  Copyright © 2016 TotalPlay. All rights reserved.
//

import UIKit
import ObjectMapper
import RealmSwift

public class MetodoPagoBean  : Object, Mappable {

    @objc public dynamic var uuid : String = UUID().uuidString
    @objc public dynamic var Metodo : String = ""
	@objc public dynamic var TipoTarjeta : String = ""
	@objc public dynamic var NombreTitular : String = ""
	@objc public dynamic var ApellidoPaternoTitular : String = ""
	@objc public dynamic var ApellidoMaternoTitular : String = ""
	@objc public dynamic var NumeroTarjeta : String = ""
	@objc public dynamic var VencimientoMes : String = ""
	@objc public dynamic var VencimientoAnio : String = ""

	required convenience public init?(map: Map) {
        self.init()
    }

    public func mapping(map: Map) {
         
		Metodo		<- map["Metodo"] 
		TipoTarjeta		<- map["TipoTarjeta"] 
		NombreTitular		<- map["NombreTitular"] 
		ApellidoPaternoTitular		<- map["ApellidoPaternoTitular"] 
		ApellidoMaternoTitular		<- map["ApellidoMaternoTitular"] 
		NumeroTarjeta		<- map["NumeroTarjeta"] 
		VencimientoMes		<- map["VencimientoMes"] 
		VencimientoAnio		<- map["VencimientoAnio"] 
	}

    override public static func primaryKey() -> String? {
        return "uuid"
    }
    
}
