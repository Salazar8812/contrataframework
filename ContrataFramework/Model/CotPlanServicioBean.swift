
//
//  ArrServicesAdditionals.swift
//  VentasTotalPlayiOS
//
//  Created by TotalPlay :v Aldynamic varado on 12/20/16.
//  Copyright © 2016 TotalPlay. All rights reserved.
//

import UIKit
import ObjectMapper
import RealmSwift

public class CotPlanServicioBean : Object, Mappable {

    @objc public dynamic var uuid : String = UUID().uuidString
	@objc public dynamic var CategoriaServicio : String = ""
	@objc public dynamic var DN_Principal : String = ""
	@objc public dynamic var DNsActivados : String = ""
	@objc public dynamic var DP_PlanServicio : String = ""
	@objc public dynamic var EsServicioAdicional : String = ""
	@objc public dynamic var EstatusActivacion : String = ""
	@objc public dynamic var IdExterno_ws : String = ""
	@objc public dynamic var IpFijasActivadas : String = ""
	@objc public dynamic var NombrePlan : String = ""
	@objc public dynamic var NombreServicio : String = ""
	@objc public dynamic var SRV_Mode : String = ""
	@objc public dynamic var Tipo : String = ""
	@objc public dynamic var DP_PromocionPlan : String = ""
    public var Cot_ServicioProducto : List<CotServicioProductoBean> = List<CotServicioProductoBean>()

	required convenience public init?(map: Map) {
        self.init()
    }

    public func mapping(map: Map) {
         
		CategoriaServicio		<- map["CategoriaServicio"] 
		DN_Principal		<- map["DN_Principal"] 
		DNsActivados		<- map["DNsActivados"] 
		DP_PlanServicio		<- map["DP_PlanServicio"] 
		EsServicioAdicional		<- map["EsServicioAdicional"] 
		EstatusActivacion		<- map["EstatusActivacion"] 
		IdExterno_ws		<- map["IdExterno_ws"] 
		IpFijasActivadas		<- map["IpFijasActivadas"] 
		NombrePlan		<- map["NombrePlan"] 
		NombreServicio		<- map["NombreServicio"] 
		SRV_Mode		<- map["SRV_Mode"] 
		Tipo		<- map["Tipo"] 
		DP_PromocionPlan		<- map["DP_PromocionPlan"]
        
        let objCotServicioProducto = List<CotServicioProductoBean>()
        for item in Cot_ServicioProducto{
            objCotServicioProducto.append(CotServicioProductoBean(value: item as Any))
        }
        self.Cot_ServicioProducto = objCotServicioProducto
		self.Cot_ServicioProducto		<- map["Cot_ServicioProducto"]
        
        if map.mappingType == MappingType.toJSON {
            var objCotServicioProducto : [CotServicioProductoBean] = []
            for item in Cot_ServicioProducto{
                objCotServicioProducto.append(CotServicioProductoBean(value: item as Any))
            }
            objCotServicioProducto        <- map["Cot_ServicioProducto"]
        }
	}

    override public static func primaryKey() -> String? {
        return "uuid"
    }
    
}
