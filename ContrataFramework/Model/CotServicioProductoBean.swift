
//
//  ArrServicesAdditionals.swift
//  VentasTotalPlayiOS
//
//  Created by Daniel García Aldynamic varado on 12/20/16.
//  Copyright © 2016 TotalPlay. All rights reserved.
//

import UIKit
import ObjectMapper
import RealmSwift

public class CotServicioProductoBean  : Object, Mappable {

    @objc public dynamic var uuid : String = UUID().uuidString
	@objc public dynamic var Cantidad : String = ""
	@objc public dynamic var CantidadDN : String = ""
	@objc public dynamic var CantidadTroncal : String = ""
	@objc public dynamic var Descuento : String = ""
	@objc public dynamic var DP_Producto : String = ""
	@objc public dynamic var DP_PromocionServicioProducto : String = ""
	@objc public dynamic var DP_ServicioProducto : String = ""
	@objc public dynamic var EsCargoUnico : String = ""
	@objc public dynamic var EsDescuento : String = ""
	@objc public dynamic var EsProductoAdicional : String = ""
	@objc public dynamic var EsProntoPago : String = ""
	@objc public dynamic var EsServicioRepetido : String = ""
	@objc public dynamic var EstatusActivacion : String = ""
	@objc public dynamic var EstatusActivacionDescripcion : String = ""
	@objc public dynamic var FechaFin : String = ""
	@objc public dynamic var FechaInicio : String = ""
	@objc public dynamic var GeneraDNs : String = ""
	@objc public dynamic var Impuesto2 : String = ""
	@objc public dynamic var Impuesto1 : String = ""
	@objc public dynamic var MontoImpuestos : String = ""
	@objc public dynamic var NombreProducto : String = ""
	@objc public dynamic var PrecioUnitario : String = ""
	@objc public dynamic var PrecioUnitario_ProntoPago : String = ""
	@objc public dynamic var PrecioUnitarioBase : String = ""
	@objc public dynamic var ProductoPadre : String = ""
	@objc public dynamic var TieneIPDinamica : String = ""
	@objc public dynamic var TieneIPFija : String = ""
	@objc public dynamic var TipoIp : String = ""
	@objc public dynamic var TipoProducto : String = ""
	@objc public dynamic var VelocidadBajada : String = ""
	@objc public dynamic var VelocidadSubida : String = ""
	@objc public dynamic var DP_PromocionPlan : String = ""
    
    required convenience public init?(map: Map) {
        self.init()
    }
    
    convenience public init(arrProductoIncluido: ArrProductosIncluido) {
        self.init()
        Cantidad = arrProductoIncluido.cantidad
        Descuento = "0.0"
        DP_PromocionServicioProducto = ""
        DP_ServicioProducto = arrProductoIncluido.id
        EsCargoUnico = arrProductoIncluido.esCargoUnico
        EsDescuento = "false"
        EsProductoAdicional = "false"
        EsProntoPago = arrProductoIncluido.esProntoPago
        Impuesto2 = arrProductoIncluido.iEPS
        Impuesto1 = arrProductoIncluido.iVA
        NombreProducto = arrProductoIncluido.nameProducto
        PrecioUnitario = arrProductoIncluido.precioBase
        PrecioUnitario_ProntoPago = arrProductoIncluido.precioProntoPago
        PrecioUnitarioBase = arrProductoIncluido.precioBase
        ProductoPadre = arrProductoIncluido.productoPadre
        TipoProducto = arrProductoIncluido.tipoProducto
        DP_PromocionPlan = ""
    }
    
    convenience public init(arrProductoAdicional: ArrProductosAdicionales) {
        self.init()
        Cantidad = arrProductoAdicional.cantidad
        Descuento = "0.0"
        DP_PromocionServicioProducto = ""
        DP_ServicioProducto = arrProductoAdicional.id
        EsCargoUnico = arrProductoAdicional.esCargoUnico
        Descuento = "false"
        EsProductoAdicional = "true"
        EsProntoPago = arrProductoAdicional.esProntoPago
        Impuesto2 = arrProductoAdicional.iEPS
        Impuesto1 = arrProductoAdicional.iVA
        NombreProducto = arrProductoAdicional.nameProducto
        PrecioUnitario = arrProductoAdicional.precioBase
        PrecioUnitario_ProntoPago = arrProductoAdicional.precioProntoPago
        PrecioUnitarioBase = arrProductoAdicional.precioBase
        ProductoPadre = arrProductoAdicional.productoPadre
        TipoProducto = arrProductoAdicional.tipoProducto
        DP_PromocionPlan = ""
    }
    
    

    public func mapping(map: Map) {
        Cantidad		<- map["Cantidad"]
        CantidadDN		<- map["CantidadDN"]
        CantidadTroncal		<- map["CantidadTroncal"]
        Descuento		<- map["Descuento"]
        DP_Producto		<- map["DP_Producto"]
        DP_PromocionServicioProducto		<- map["DP_PromocionServicioProducto"]
        DP_ServicioProducto		<- map["DP_ServicioProducto"]
        EsCargoUnico		<- map["EsCargoUnico"]
        EsDescuento		<- map["EsDescuento"]
        EsProductoAdicional		<- map["EsProductoAdicional"]
        EsProntoPago		<- map["EsProntoPago"]
        EsServicioRepetido		<- map["EsServicioRepetido"]
        EstatusActivacion		<- map["EstatusActivacion"]
        EstatusActivacionDescripcion		<- map["EstatusActivacionDescripcion"]
        FechaFin		<- map["FechaFin"]
        FechaInicio		<- map["FechaInicio"]
        GeneraDNs		<- map["GeneraDNs"]
        Impuesto2		<- map["Impuesto2"]
        Impuesto1		<- map["Impuesto1"]
        MontoImpuestos		<- map["MontoImpuestos"]
        NombreProducto		<- map["NombreProducto"]
        PrecioUnitario		<- map["PrecioUnitario"]
        PrecioUnitario_ProntoPago		<- map["PrecioUnitario_ProntoPago"]
        PrecioUnitarioBase		<- map["PrecioUnitarioBase"]
        ProductoPadre		<- map["ProductoPadre"]
        TieneIPDinamica		<- map["TieneIPDinamica"]
        TieneIPFija		<- map["TieneIPFija"]
        TipoIp		<- map["TipoIp"]
        TipoProducto		<- map["TipoProducto"]
        VelocidadBajada		<- map["VelocidadBajada"] 
        VelocidadSubida		<- map["VelocidadSubida"] 
        DP_PromocionPlan		<- map["DP_PromocionPlan"]
	}
    
    override public static func primaryKey() -> String? {
        return "uuid"
    }

}
