
//
//  ArrServicesAdditionals.swift
//  VentasTotalPlayiOS
//
//  Created by Daniel García Aldynamic varado on 12/20/16.
//  Copyright © 2016 TotalPlay. All rights reserved.
//

import UIKit
import ObjectMapper
import RealmSwift

public class ArrProductosAdicionales  : Object, Mappable, AdicionalesBean {
    
    public func getID() -> String {
        return id
    }
    public func getAdicionalType() -> Int {
        return AddicionalBeanType.PRODUCT_ADDITIONAL
    }
    public func getAdicionalName() -> String {
        return nombre
    }
    public func getPrecioBase() -> Double {
        return precioBase != "" ? Double(precioBase)! : 0
    }
    public func getPrecioProntoPago() -> Double {
        return precioProntoPago != "" ? Double(precioProntoPago)! : 0
    }
    public func getCantidad() -> Double {
        return cantidad != "" ? Double(cantidad)! : 0
    }
    public func getIVA() -> Double {
        return iVA != "" ? Double(iVA)! : 0
    }
    public func getIEPS() -> Double {
        return iEPS != "" ? Double(iEPS)! : 0
    }
    
    @objc public dynamic var uuid : String = UUID().uuidString
	@objc public dynamic var esProntoPago : String = ""
	@objc public dynamic var id : String = ""
	@objc public dynamic var nombre : String = ""
	@objc public dynamic var agrupacionAddon : String = ""
	@objc public dynamic var cantidadDN : String = ""
	@objc public dynamic var cantidadTroncal : String = ""
	@objc public dynamic var comentario : String = ""
	@objc public dynamic var esAutomaticoCiudad : String = ""
	@objc public dynamic var esCargoUnico : String = ""
	@objc public dynamic var estatus : String = ""
	@objc public dynamic var esVisible : String = ""
	@objc public dynamic var idBrmArrear : String = ""
	@objc public dynamic var idBrmCU : String = ""
	@objc public dynamic var idBrmForward : String = ""
	@objc public dynamic var iEPS : String = ""
	@objc public dynamic var iVA : String = ""
	@objc public dynamic var maximoAgregar : String = ""
	@objc public dynamic var nombreEditable : String = ""
	@objc public dynamic var planDescuentoId : String = ""
	@objc public dynamic var plazo : String = ""
	@objc public dynamic var precioBase : String = ""
	@objc public dynamic var precioEditable : String = ""
	@objc public dynamic var precioProntoPago : String = ""
	@objc public dynamic var productoPadre : String = ""
	@objc public dynamic var tipoProducto : String = ""
	@objc public dynamic var tipoProductoTexto : String = ""
	@objc public dynamic var idProducto : String = ""
	@objc public dynamic var nameProducto : String = ""
	@objc public dynamic var productoId : String = ""
	@objc public dynamic var ciudad : String = ""
	@objc public dynamic var velocidadSubida : String = ""
	@objc public dynamic var velocidadBajada : String = ""
	@objc public dynamic var tieneIPDinamica : String = ""
	@objc public dynamic var tieneIPFija : String = ""
	@objc public dynamic var tieneSTBAdicional : String = ""
	@objc public dynamic var esCCTV : String = ""
	@objc public dynamic var esWiFi : String = ""
	@objc public dynamic var cantidad : String = ""
	@objc public dynamic var estatusProducto : String = ""
	@objc public dynamic var fechaInicio : String = ""
	@objc public dynamic var fechaFin : String = ""
	@objc public dynamic var comentarioProducto : String = ""
    @objc public dynamic var priceApp : String = ""
    @objc public dynamic var image : String = ""
    public var ArrProductosConvivencia : List<ArrProductosConvivenciaBean> = List<ArrProductosConvivenciaBean>()

    public var isSelected : Bool = false
    
	required public convenience init?(map: Map) {
        self.init()
    }
    
    public func attributeArray() -> [NSObject] {
        return Array(self.ArrProductosConvivencia)
    }
    
    public func attrbuteTitleSection() -> String {
        return self.agrupacionAddon
    }


    public func mapping(map: Map) {
         
		esProntoPago		<- map["EsProntoPago"]
		id		<- map["Id"]
		nombre		<- map["Nombre"]
		agrupacionAddon		<- map["AgrupacionAddon"]
		cantidadDN		<- map["CantidadDN"]
		cantidadTroncal		<- map["CantidadTroncal"]
		comentario		<- map["Comentario"]
		esAutomaticoCiudad		<- map["EsAutomaticoCiudad"]
		esCargoUnico		<- map["EsCargoUnico"]
		estatus		<- map["Estatus"]
		esVisible		<- map["EsVisible"]
		idBrmArrear		<- map["IdBrmArrear"]
		idBrmCU		<- map["IdBrmCU"]
		idBrmForward		<- map["IdBrmForward"]
		iEPS		<- map["IEPS"]
		iVA		<- map["IVA"]
		maximoAgregar		<- map["MaximoAgregar"]
		nombreEditable		<- map["NombreEditable"]
		planDescuentoId		<- map["PlanDescuentoId"]
		plazo		<- map["Plazo"]
		precioBase		<- map["PrecioBase"]
		precioEditable		<- map["PrecioEditable"]
		precioProntoPago		<- map["PrecioProntoPago"]
		productoPadre		<- map["ProductoPadre"]
		tipoProducto		<- map["TipoProducto"]
		tipoProductoTexto		<- map["TipoProductoTexto"]
		idProducto		<- map["IdProducto"]
		nameProducto		<- map["NameProducto"]
		productoId		<- map["ProductoId"]
		ciudad		<- map["Ciudad"]
		velocidadSubida		<- map["VelocidadSubida"]
		velocidadBajada		<- map["VelocidadBajada"]
		tieneIPDinamica		<- map["TieneIPDinamica"]
		tieneIPFija		<- map["TieneIPFija"]
		tieneSTBAdicional		<- map["TieneSTBAdicional"]
		esCCTV		<- map["EsCCTV"]
		esWiFi		<- map["EsWiFi"]
		cantidad		<- map["Cantidad"]
		estatusProducto		<- map["EstatusProducto"]
		fechaInicio		<- map["FechaInicio"]
		fechaFin		<- map["FechaFin"]
		comentarioProducto		<- map["ComentarioProducto"]
        priceApp	<- map["priceApp"]
        image    <- map["URLApp__c"]
        
        
        let objArrProductosConvivencia = List<ArrProductosConvivenciaBean>()
        for item in ArrProductosConvivencia{
            objArrProductosConvivencia.append(ArrProductosConvivenciaBean(value: item as Any))
        }
        self.ArrProductosConvivencia = objArrProductosConvivencia
		self.ArrProductosConvivencia		<- map["ArrProductosConvivencia"]
	}
    
    override public static func primaryKey() -> String? {
        return "uuid"
    }

}
