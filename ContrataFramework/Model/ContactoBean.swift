
//
//  ArrServicesAdditionals.swift
//  VentasTotalPlayiOS
//
//  Created by Daniel García Aldynamic varado on 12/20/16.
//  Copyright © 2016 TotalPlay. All rights reserved.
//

import UIKit
import ObjectMapper 
import RealmSwift

public class ContactoBean  :  Object, Mappable {

    @objc public dynamic var uuid : String = UUID().uuidString
    @objc public dynamic var TipoPersona : String = ""
	@objc public dynamic var Nombre : String = ""
	@objc public dynamic var ApellidoPaterno : String = ""
	@objc public dynamic var ApellidoMaterno : String = ""
	@objc public dynamic var Telefono : String = ""
	@objc public dynamic var OtroTelefono : String = ""
	@objc public dynamic var Celular : String = ""
	@objc public dynamic var CorreoElectronico : String = ""
	@objc public dynamic var OtroCorreoElectronico : String = ""
	@objc public dynamic var RFC : String = ""
	@objc public dynamic var RazonSocial : String = ""
    @objc public dynamic var MedioContacto : String = ""
	@objc public dynamic var FechaNacimiento : String = ""

	required convenience public init?(map: Map) {
        self.init()
    }

    public func mapping(map: Map) {
         
		TipoPersona		<- map["TipoPersona"] 
		Nombre		<- map["Nombre"] 
		ApellidoPaterno		<- map["ApellidoPaterno"] 
		ApellidoMaterno		<- map["ApellidoMaterno"] 
		Telefono		<- map["Telefono"] 
		OtroTelefono		<- map["OtroTelefono"] 
		Celular		<- map["Celular"] 
		CorreoElectronico		<- map["CorreoElectronico"] 
		OtroCorreoElectronico		<- map["OtroCorreoElectronico"] 
		RFC		<- map["RFC"] 
		RazonSocial		<- map["RazonSocial"]
        MedioContacto        <- map["MedioContacto"]
		FechaNacimiento		<- map["FechaNacimiento"]
	}
    
    override public static func primaryKey() -> String? {
        return "uuid"
    }

}
