
//
//  ArrServicesAdditionals.swift
//  VentasTotalPlayiOS
//
//  Created by Daniel García Aldynamic varado on 12/20/16.
//  Copyright © 2016 TotalPlay. All rights reserved.
//

import UIKit
import ObjectMapper
import RealmSwift

public class VendedorBean  : Object, Mappable {

    @objc public dynamic var uuid : String = UUID().uuidString
	@objc public dynamic var IdEmpleado : String = ""
	@objc public dynamic var Canal : String = ""
	@objc public dynamic var SubCanal : String = ""
	@objc public dynamic var FolioContrato : String = ""
	@objc public dynamic var IdDispositivo : String = ""
	@objc public dynamic var IdDistrital : String = ""
	@objc public dynamic var IdEmpleadoDistrital : String = ""
    
    
    @objc public dynamic var OrigenApp : String = ""
    @objc public dynamic var EsComisionable : String = ""
    @objc public dynamic var IMEI : String = ""
    @objc public dynamic var ModeloCelular : String = ""
    @objc public dynamic var EmpleadoDistribuidor : String = ""
    @objc public dynamic var UsuarioLogueado : String = ""
    @objc public dynamic var aprobarVentaExpress : String = ""
    
    @objc public dynamic var ventaExpress : String = ""

	required convenience public init?(map: Map) {
        self.init()
    }

    public func mapping(map: Map) {
         
		IdEmpleado		<- map["IdEmpleado"] 
		Canal		<- map["Canal"] 
		SubCanal		<- map["SubCanal"] 
		FolioContrato		<- map["FolioContrato"] 
		IdDispositivo		<- map["IdDispositivo"] 
		IdDistrital		<- map["IdDistrital"] 
		IdEmpleadoDistrital		<- map["IdEmpleadoDistrital"]
        OrigenApp		<- map["OrigenApp"]
        EsComisionable		<- map["EsComisionable"]
        IMEI		<- map["IMEI"]
        ModeloCelular		<- map["ModeloCelular"]
        EmpleadoDistribuidor		<- map["Empleado_Distribuidor"]
        UsuarioLogueado		<- map["Usuario_Logueado"]
        ventaExpress		<- map["VentaExpress"]
        aprobarVentaExpress		<- map["AprobarVentaExpress"]

        
    }
    
    override public static func primaryKey() -> String? {
        return "uuid"
    }


}
